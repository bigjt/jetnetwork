#include <cstdio>
#include <unistd.h>
#include <string.h>
#include <ctime>
#include <chrono>
#include <thread>
#include <iostream>


#include "../JetNetwork/SocketUtilsImpl.hpp"
#include "../JetNetwork/Sockets/TcpSocket.hpp"
#include "../JetNetwork/Sockets/UdpSocket.hpp"

typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::milliseconds milliseconds;


IpAddress linux_ip = IpAddress("192.168.71.26");
IpAddress windows_ip = IpAddress("192.168.71.20");
IpEndpoint linux_udp_ep = IpEndpoint(linux_ip, 2222);
IpEndpoint tcp_connect_ep = IpEndpoint(windows_ip, 4444);
IpEndpoint tcp_listen_ep = IpEndpoint(linux_ip, 4444);
IpAddress multicast_addr = IpAddress(224, 224, 0, 100);
uint16_t multicast_rx_port = 9999;


bool running = true;
constexpr uint32_t REPORT_TEST_DATA_INTERVAL_MILLI = 1000;

void TestSocketUtils()
{
    IpAddress ip1 = IpAddress("192.168.71.26");
    IpAddress ip2 = IpAddress("255.255.255.1");
    IpAddress ip3 = IpAddress("192.168.71.265");
    IpAddress ip4 = IpAddress("192.168.80.1");

    std::vector<IpAddress> ips_to_test{ ip1, ip2, ip3, ip4,
        IpAddress::IP_BROADCAST,  IpAddress::IP_ANY, IpAddress::IP_LOOPBACK, IpAddress::IP_NONE };

    auto adapters = socket_utils::get_network_ipv4_adapters();

    for (auto &ip : ips_to_test)
    {
        auto ip_addr = socket_utils::ip_to_string(ip);
        auto broadcast_addr = socket_utils::ip_to_string(socket_utils::get_broadcast_addr(ip, adapters));
        bool is_useable = socket_utils::is_ip_address_useable(ip, adapters);

        printf("ip: %s bcast: %s useable: %d\n", ip_addr.c_str(), broadcast_addr.c_str(), is_useable);
    }

    printf("default tcp sndbuf: %d rcvbuf: %d\n", socket_utils::default_tcp_sendbuf_size(), socket_utils::default_tcp_recvbuf_size());
    printf("default udp sndbuf: %d rcvbuf: %d\n", socket_utils::default_udp_sendbuf_size(), socket_utils::default_udp_recvbuf_size());
}

void UdpEchoTest()
{
    UdpSocket udp_socket;
    udp_socket.EnableBlocking(false);
    udp_socket.Bind(linux_udp_ep);
    //  udp_socket.EnableBroadcast(true);

    std::string data_to_send("hello from linux world");
    uint8_t recv_buffer[1024];
    auto udp_last_time = Clock::now();

    // setup IP. wait until we get a packet to start the echo.
    IpEndpoint remote_udp_host = IpEndpoint(IpAddress::IP_NONE, 2222);

    auto msg_count_print_timer = Clock::now();
    uint64_t data_in_count = 0;
    uint64_t data_out_count = 0;

    while (running)
    {
        //auto ms_since_last_udp = std::chrono::duration_cast<milliseconds>(Clock::now() - udp_last_time);
        //if (ms_since_last_udp.count() > 500)
        {
            if (remote_udp_host.IpAddr().HostOrderAddress() != IpAddress::IP_NONE.HostOrderAddress())
            {
                //printf("UDP send: %s dt:%d\n", data_to_send.c_str(), ms_since_last_udp.count());
                udp_last_time = Clock::now();
                udp_socket.Send((void*)data_to_send.c_str(), data_to_send.length(), remote_udp_host);
                ++data_out_count;
            }
        }

        size_t bytes_recv = 0;
        //  IpEndpoint remote_udp_host2 = { IP_ANY, AnyPort };
        if (udp_socket.Receive(recv_buffer, sizeof(recv_buffer), bytes_recv, remote_udp_host) == SocketStatus::Done)
        {
            ++data_in_count;
            //std::string data_in_str(reinterpret_cast<char const*>(recv_buffer), bytes_recv);
            //printf("UDP recv: %s \n", data_in_str.c_str());
        }

        auto ms_since_last_print = std::chrono::duration_cast<milliseconds>(Clock::now() - msg_count_print_timer);
        if (ms_since_last_print.count() >= REPORT_TEST_DATA_INTERVAL_MILLI)
        {
            msg_count_print_timer = Clock::now();
            printf("UDPEcho in:%llu out:%llu\n", data_in_count, data_out_count);
        }


        const int MILLISECS_TO_SLEEP = 1;
        usleep(MILLISECS_TO_SLEEP * 1000);
    }
}

void MulticastTest()
{
    UdpSocket udp_socket;
    udp_socket.EnableBlocking(false);
    udp_socket.Bind(IpEndpoint(IpAddress::IP_ANY, multicast_rx_port)); //I can't recv when I bind to the net adapter (linux_ip)?? have to use Inaddrany
    udp_socket.JoinMulticastGroup(multicast_addr);
    udp_socket.SetMulticastTimeToLive(MutlicastTtlThreshold::SameSubnet);
    
    std::string data_to_send("hello from windows world");
    uint8_t recv_buffer[1024];

    auto msg_count_print_timer = Clock::now();
    uint64_t data_in_count = 0;
    uint64_t data_out_count = 0;

    SocketStatus last_socket_status = SocketStatus::Error;
    IpEndpoint mcast_ep(multicast_addr, multicast_rx_port);

    // just testing connect to a multicast EP.
    //   This will prevent RX of multicast group packets on this port.
    //   Same goes for calling connect with a broadcast address
    //   This call will make any bcast or mcast socket a sender only (using connect() saves a few system calls within ::send()).
    //udp_socket.Connect(mcast_ep);

    while (running)
    {
        if (udp_socket.IsConnected())
            udp_socket.Send((void*)data_to_send.c_str(), data_to_send.length());
        else
            udp_socket.Send((void*)data_to_send.c_str(), data_to_send.length(), mcast_ep);
        
        ++data_out_count;

        size_t bytes_recv = 0;
        IpEndpoint remote_udp_host2;
        if ((last_socket_status = udp_socket.Receive(recv_buffer, sizeof(recv_buffer), bytes_recv, remote_udp_host2)) == SocketStatus::Done)
        {
           // auto ip_str = remote_udp_host2.IpAddr().ToString();
           // auto rx_ep_str = remote_udp_host2.ToString();
            ++data_in_count;
            //std::string data_in_str(reinterpret_cast<char const*>(recv_buffer), bytes_recv);
            //printf("UDP recv: %s \n", data_in_str.c_str());
        }

        auto ms_since_last_print = std::chrono::duration_cast<milliseconds>(Clock::now() - msg_count_print_timer);
        if (ms_since_last_print.count() >= REPORT_TEST_DATA_INTERVAL_MILLI)
        {
            msg_count_print_timer = Clock::now();
            printf("MulticastTest in:%llu out:%llu\n", data_in_count, data_out_count);
        }

        const int MILLISECS_TO_SLEEP = 1;
        usleep(MILLISECS_TO_SLEEP * 1000);
    }
}

void TcpListenAndConnectTest()
{
    TcpSocket tcp_socket_test;
    TcpListenSocket listen_socket;
    tcp_socket_test.EnableBlocking(false);
    std::string data_to_send("hello from linux world");
    std::string expected_data("hello from windows world");

    auto msg_count_print_timer = Clock::now();
    uint64_t data_in_count = 0;
    uint64_t data_out_count = 0;

    int conn_state = 0;
    auto last_time = Clock::now();

    uint8_t recv_buffer[1024];

    while (running)
    {
        switch (conn_state)
        {
        case 0:
            ++conn_state;
            break;
        case 1:
        {
            auto sock_stat = tcp_socket_test.Connect(tcp_connect_ep, 1000);
            //auto sock_stat = socket_utils::listen_and_connect_single_client(listen_socket,
            //    tcp_listen_ep, tcp_socket_test, 1000);

            if (sock_stat == SocketStatus::Done)
            {
                printf("connected \n");
                ++conn_state;
            }
            else if (sock_stat == SocketStatus::Error)
            {
                tcp_socket_test.Disconnect();
                conn_state = 0;
            }
            break;
        }
        case 2:
            milliseconds ms = std::chrono::duration_cast<milliseconds>(Clock::now() - last_time);
            //printf("TCP send: %s %d dt:%d\n", data_to_send.c_str(), ++sent_count, ms.count());

            size_t bytes_sent = 0;
            auto send_status = tcp_socket_test.Send(data_to_send.c_str(), data_to_send.length(), bytes_sent);

            if (send_status == SocketStatus::Done)
            {
                ++data_out_count;
            }

            if (send_status == SocketStatus::Partial)
            {
                ++data_out_count;
                printf("partial send %d/%d \n", bytes_sent, data_to_send.length());
            }

            if (send_status == SocketStatus::NotReady)
                printf("socket not ready \n");

            if (send_status == SocketStatus::Disconnected)
            {
                printf("clean disconnect \n");
                conn_state = 0;
            }

            if (send_status == SocketStatus::Error)
            {
                printf("error disconnect \n");
                tcp_socket_test.Disconnect();
                conn_state = 0;
            }

            // do some recv too.
            size_t bytes_recv = 0;
            auto recv_status = tcp_socket_test.Receive(recv_buffer, expected_data.length(), bytes_recv);

            if (recv_status == SocketStatus::Done)
                ++data_in_count;

            if (recv_status == SocketStatus::Partial)
            {
                ++data_in_count;
                printf("recv partial %d/%d \n", (int)bytes_recv, (int)expected_data.length());
            }

            if (recv_status == SocketStatus::NotReady)
                printf("recv socket not ready \n");

            if (recv_status == SocketStatus::Disconnected)
            {
                printf("recv clean disconnect \n");
                printf("err: %s \n", socket_utils::error_code_to_descriptive_string(tcp_socket_test.LastSocketErrorCode()).c_str());
                conn_state = 0;
            }

            if (recv_status == SocketStatus::Error)
            {
                printf("recv error disconnect \n");
                printf("err: %s \n", socket_utils::error_code_to_descriptive_string(tcp_socket_test.LastSocketErrorCode()).c_str());
                tcp_socket_test.Disconnect();
                conn_state = 0;
            }

            break;
        }

        if (tcp_socket_test.IsConnected())
        {
            auto ms_since_last_print = std::chrono::duration_cast<milliseconds>(Clock::now() - msg_count_print_timer);
            if (ms_since_last_print.count() >= REPORT_TEST_DATA_INTERVAL_MILLI)
            {
                msg_count_print_timer = Clock::now();
                printf("TcpLC in:%llu out:%llu\n", data_in_count, data_out_count);
            }
        }

        last_time = Clock::now();
        const int MILLISECS_TO_SLEEP = 1;
        usleep(MILLISECS_TO_SLEEP * 1000);
    }
}

void TcpSendRecvUtilityTest()
{
    IpEndpoint test_listen_ep = IpEndpoint(linux_ip, 6666);
    IpEndpoint test_connect_ep = IpEndpoint(windows_ip, 6666);
    TcpSocket tcp_socket_test;
    TcpListenSocket listen_socket;
    tcp_socket_test.EnableBlocking(false); //TODO: break this test out int diff thread blocking/nonblocking...

    int conn_state = 0;
    auto last_time = Clock::now();
    auto msg_count_print_timer = Clock::now();
    uint64_t data_in_count = 0;
    uint64_t data_out_count = 0;

    constexpr uint32_t message_data_size = 1048 * 12;

    std::unique_ptr<uint8_t[]> data_to_send(new uint8_t[message_data_size]);
    memset(data_to_send.get(), 8, message_data_size);

    std::unique_ptr<uint8_t[]> recv_buffer(new uint8_t[message_data_size]);
    SocketStatus last_socket_status = SocketStatus::Error;

    std::thread send_thread = std::thread([&]()
    {
        while (running)
        {
            if (!tcp_socket_test.IsConnected())
            {
                usleep(100000);
                continue;
            }

            size_t bytes_sent = 0;
            auto send_result = socket_utils::try_send_all(tcp_socket_test,
                (uint8_t*)data_to_send.get(), message_data_size, bytes_sent, 3, 20);

            auto send_status = send_result.first;
            auto retry_count = send_result.second;

            if (retry_count > 0)
                printf("send retry cnt %d \n", retry_count);

            if (send_status == SocketStatus::Done)
            {
                ++data_out_count;
                //if (data_out_count % 5000 == 0)
                //    tcp_socket_test.Disconnect();
            }

            if (send_status == SocketStatus::Partial)
            {
                ++data_out_count;
                printf("partial send %d/%d \n", bytes_sent, message_data_size);
            }

            if (send_status == SocketStatus::NotReady)
                printf("send socket not ready \n");

            if (send_status == SocketStatus::Disconnected ||
                send_status == SocketStatus::Error)
            {
                printf("send disconnect %s \n",
                    //socket_utils::error_code_to_descriptive_string(socket_impl::last_system_error_code()).c_str());
                    socket_utils::error_code_to_descriptive_string(tcp_socket_test.LastSocketErrorCode()).c_str());
                tcp_socket_test.Disconnect();
                conn_state = 0;
            }

            if (send_status == SocketStatus::Error)
                printf("send error disconnect \n");

            const int MILLISECS_TO_SLEEP = 1;
            usleep(MILLISECS_TO_SLEEP * 1000);
        }
    });

    while (running)
    {
        switch (conn_state)
        {
        case 0:
            ++conn_state;
            break;
        case 1:
        {
            //auto sock_stat = socket_utils::listen_and_connect_single_client(listen_socket,
            //    test_listen_ep, tcp_socket_test, 1000);
            auto sock_stat = tcp_socket_test.Connect(test_connect_ep, 1000);

            if (sock_stat == SocketStatus::Done)
            {
                printf("connected \n");
                ++conn_state;
            }
            else if (sock_stat == SocketStatus::Error)
            {
                printf("connect err: %s \n", socket_utils::error_code_to_descriptive_string(tcp_socket_test.LastSocketErrorCode()).c_str());
                tcp_socket_test.Disconnect();
                conn_state = 0;
                data_in_count = 0;
                data_out_count = 0;
                usleep(1000 * 1000); // just giving some extra time after an error... not really needed if using Connect with a timeout value.
            }
            else
            {
                printf("connect dummy print... \n");
            }
            break;
        }
        case 2:
            milliseconds ms = std::chrono::duration_cast<milliseconds>(Clock::now() - last_time);

            // do some recv too.
            size_t bytes_recv = 0;
            auto recv_status = socket_utils::tcp_recv_with_timeout(tcp_socket_test,
                recv_buffer.get(), message_data_size, bytes_recv, 1000);

            if (recv_status == SocketStatus::Done)
                ++data_in_count;

            if (recv_status == SocketStatus::Partial)
            {
                ++data_in_count;
               // printf("recv partial %d/%d \n", (int)bytes_recv, (int)message_data_size);
            }

            if (recv_status == SocketStatus::NotReady)
                printf("recv socket not ready \n");

            if (recv_status == SocketStatus::Disconnected ||
                recv_status == SocketStatus::Error)
            {
                printf("recv disconnect %s \n",
                    socket_utils::error_code_to_descriptive_string(tcp_socket_test.LastSocketErrorCode()).c_str());
                tcp_socket_test.Disconnect();
                conn_state = 0;
            }

            if (recv_status == SocketStatus::Error)
                printf("recv error disconnect \n");

            break;
        }

        if (tcp_socket_test.IsConnected())
        {
            auto ms_since_last_print = std::chrono::duration_cast<milliseconds>(Clock::now() - msg_count_print_timer);
            if (ms_since_last_print.count() >= REPORT_TEST_DATA_INTERVAL_MILLI)
            {
                msg_count_print_timer = Clock::now();
                //printf("TcpLC in:%llu out:%llu\n", data_in_count, data_out_count);
            }
        }


        last_time = Clock::now();

        // don't sleep if connected
        // with non-blocking configured, this will test tcp_recv_with_timeout performance.
        if (tcp_socket_test.IsConnected())
        {
            continue;
        }

        const int MILLISECS_TO_SLEEP = 1;
        usleep(MILLISECS_TO_SLEEP * 1000);
    }

    if (send_thread.joinable())
        send_thread.join();
}

void UdpTestMultipleConnectOnSameSocket()
{
    uint8_t data_buf[1024];

    UdpSocket udp_socket;
    udp_socket.Bind({ IpAddress::IP_ANY, 2021 });

    UdpSocket udp_socket1;
    udp_socket1.Bind({ IpAddress::IP_ANY, 2022 });

    UdpSocket udp_socket2;
    udp_socket2.Bind({ IpAddress::IP_ANY, 2024 });

    size_t loop_count = 0;

    while (running)
    {
        size_t bytes_recv = 0;
        IpEndpoint recv_ep;

        if (udp_socket.Receive(data_buf, sizeof(data_buf), bytes_recv, recv_ep) == SocketStatus::Done)
        {
            printf("%d %s got data from %s\n", loop_count, data_buf, recv_ep.ToString().c_str());
        }

        if (udp_socket1.Receive(data_buf, sizeof(data_buf), bytes_recv, recv_ep) == SocketStatus::Done)
        {
            printf("%d %s got data from %s\n", loop_count, data_buf, recv_ep.ToString().c_str());
        }

        if (udp_socket2.Receive(data_buf, sizeof(data_buf), bytes_recv, recv_ep) == SocketStatus::Done)
        {
            printf("%d %s got data from %s\n", loop_count, data_buf, recv_ep.ToString().c_str());
        }

        ++loop_count;

        const int MILLISECS_TO_SLEEP = 1;
        usleep(MILLISECS_TO_SLEEP * 1000);
    }
}


void JoinThread(std::thread &thread)
{
    if (thread.joinable())
        thread.join();
}


int main()
{
    printf("hello from JetNetworkLinux!\n");

    TestSocketUtils();

    //std::thread udp_test_thread(UdpEchoTest);
    //std::thread tcp_test_thread(TcpListenAndConnectTest);
    //std::thread mcast_test_thread(MulticastTest);
    //std::thread tcp_utils_test_thread(TcpSendRecvUtilityTest);
    std::thread udp_connect_test(UdpTestMultipleConnectOnSameSocket);

    auto test_adapters = socket_utils::get_network_ipv4_adapters();

    // wait until console data is entered, then kill the program
    // (text + enter key)
    std::string some_data;
    std::cin >> some_data;

    running = false;

    //JoinThread(udp_test_thread);
    //JoinThread(tcp_test_thread);
    //JoinThread(mcast_test_thread);
    //JoinThread(tcp_utils_test_thread);
    JoinThread(udp_connect_test);
    return 0;
}