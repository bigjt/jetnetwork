// JetNetworkWinCppConsole.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <cstdio>
#include <string.h>
#include <ctime>
#include <chrono>
#include <thread>

#include "../JetNetwork/SocketUtilsImpl.hpp"
#include "../JetNetwork/Sockets/TcpSocket.hpp"
#include "../JetNetwork/Sockets/UdpSocket.hpp"

typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::milliseconds milliseconds;

IpAddress linux_ip = IpAddress("192.168.71.26");
IpAddress windows_ip = IpAddress("192.168.71.20");
IpEndpoint linux_udp_ep = IpEndpoint(linux_ip, 2222);
IpEndpoint tcp_connect_ep = IpEndpoint(linux_ip, 4444);
IpEndpoint tcp_listen_ep = IpEndpoint(windows_ip, 4444);
IpAddress multicast_addr = IpAddress(224, 224, 0, 100);
uint16_t multicast_rx_port = 9999;

bool running = true;
constexpr uint32_t REPORT_TEST_DATA_INTERVAL_MILLI = 1000;

//TODO: need to perform the same tests on both OS's in some cases.
//      should go ahead and name them LinuxSideTestX... WindowSideTextX.

void TestSocketUtils()
{
    IpAddress ip1 = IpAddress("192.168.71.26");
    IpAddress ip2 = IpAddress("255.255.255.1");
    IpAddress ip3 = IpAddress("192.168.71.265");
    IpAddress ip4 = IpAddress("192.168.80.1");
    IpAddress ip5 = IpAddress("224.224.100.255");

    std::vector<IpAddress> ips_to_test {ip1, ip2, ip3, ip4, ip5,
        IpAddress::IP_BROADCAST,  IpAddress::IP_ANY, IpAddress::IP_LOOPBACK, IpAddress::IP_NONE };

    auto adapters = socket_utils::get_network_ipv4_adapters();

    for (auto &ip : ips_to_test)
    {
        auto ip_addr = socket_utils::ip_to_string(ip);
        auto broadcast_addr = socket_utils::ip_to_string(socket_utils::get_broadcast_addr(ip, adapters));
        bool is_useable = socket_utils::is_ip_address_useable(ip, adapters);

        printf("ip: %s bcast: %s useable: %d\r\n", ip_addr.c_str(), broadcast_addr.c_str(), is_useable);
        printf("ip: %s ucast %d bcast %d mcast %d loop %d\r\n", ip_addr.c_str(), ip.IsUnicast(), ip.IsBroadcast(), ip.IsMulticast(), ip.IsLoopback());
    }

    // Make sure to check if remote address is in the current subnet before calling send_arp_request. 
    //  ARP requests are only valid within local subnets. Sending an ARP to another net will 
    //  waste 3 - 4 seconds just to return false.
    // Specify a local address in send_arp_request, otherwise the subnet check is kinda worthless. 
    auto arp_timer = Clock::now();
    auto arp_result = socket_utils::send_arp_request(linux_ip, windows_ip);
    auto arp_time = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now() - arp_timer);

    // TODO: Add util function to check if 2 IPs are in the same subnet. This will require a query of the subnet mask set on the adapter...
    //       OR simply a user provided subnet mask. 
    //       When I do make one that accepts netadapters, add a utils function IpAddress get_subnet_mask(...)
    bool same_subnet1 = socket_utils::ip_addresses_share_subnet(linux_ip, windows_ip, IpAddress(255, 255, 255, 0));
    bool same_subnet2 = socket_utils::ip_addresses_share_subnet(linux_ip, IpAddress::IP_ANY, IpAddress(255, 255, 255, 0));  
    bool same_subnet3 = socket_utils::ip_addresses_share_subnet(IpAddress("172.21.241.175"), windows_ip, IpAddress(255, 255, 255, 0));

    printf("default tcp sndbuf: %d rcvbuf: %d \n", socket_utils::default_tcp_sendbuf_size(), socket_utils::default_tcp_recvbuf_size());
    printf("default udp sndbuf: %d rcvbuf: %d \n", socket_utils::default_udp_sendbuf_size(), socket_utils::default_udp_recvbuf_size());
    return;
}

void UdpEchoTest()
{
    UdpSocket udp_socket;
    udp_socket.EnableBlocking(false);
    udp_socket.Bind(IpEndpoint(windows_ip, 2222));

    std::string data_to_send("hello from windows world");
    uint8_t recv_buffer[1024];
    auto udp_last_time = Clock::now();

    auto msg_count_print_timer = Clock::now();
    uint64_t data_in_count = 0;
    uint64_t data_out_count = 0;

    SocketStatus last_socket_status = SocketStatus::Error;

    while (running)
    {
        //auto ms_since_last_udp = std::chrono::duration_cast<milliseconds>(Clock::now() - udp_last_time);
        //if (ms_since_last_udp.count() > 500)
        {
            //printf("UDP send: %s dt:%d\n", data_to_send.c_str(), ms_since_last_udp.count());
            udp_last_time = Clock::now();
           // auto ipstr = linux_udp_ep.IpAddr().ToString();
            udp_socket.Send((void*)data_to_send.c_str(), data_to_send.length(), linux_udp_ep);
            ++data_out_count;
        }

        size_t bytes_recv = 0;
        IpEndpoint remote_udp_host2 = { IpAddress::IP_ANY, AnyPort };
        if ((last_socket_status = udp_socket.Receive(recv_buffer, sizeof(recv_buffer), bytes_recv, remote_udp_host2)) == SocketStatus::Done)
        {
            ++data_in_count;
            //std::string data_in_str(reinterpret_cast<char const*>(recv_buffer), bytes_recv);
            //printf("UDP recv: %s \n", data_in_str.c_str());
        }

        auto ms_since_last_print = std::chrono::duration_cast<milliseconds>(Clock::now() - msg_count_print_timer);
        if (ms_since_last_print.count() >= REPORT_TEST_DATA_INTERVAL_MILLI)
        {
            msg_count_print_timer = Clock::now();
            printf("UDPEcho in:%llu out:%llu\n", data_in_count, data_out_count);
        }

        //const int MILLISECS_TO_SLEEP = 1;
        //Sleep(MILLISECS_TO_SLEEP);
        std::this_thread::sleep_for(std::chrono::nanoseconds(1100));
    }
}

void MulticastTest()
{
    UdpSocket udp_socket;
    udp_socket.EnableBlocking(false);
    udp_socket.Bind(IpEndpoint(windows_ip, multicast_rx_port));
    udp_socket.JoinMulticastGroup(multicast_addr);
    udp_socket.SetMulticastTimeToLive(MutlicastTtlThreshold::SameSubnet);

    std::string data_to_send("hello from windows world");
    uint8_t recv_buffer[1024];

    auto msg_count_print_timer = Clock::now();
    uint64_t data_in_count = 0;
    uint64_t data_out_count = 0;

    SocketStatus last_socket_status = SocketStatus::Error;
    IpEndpoint mcast_ep(multicast_addr, multicast_rx_port);

    while (running)
    {
        udp_socket.Send((void*)data_to_send.c_str(), data_to_send.length(), mcast_ep);
        ++data_out_count;

        size_t bytes_recv = 0;
        IpEndpoint remote_udp_host2;
        if ((last_socket_status = udp_socket.Receive(recv_buffer, sizeof(recv_buffer), bytes_recv, remote_udp_host2)) == SocketStatus::Done)
        {
           // auto ip_str = remote_udp_host2.IpAddr().ToString();
           // auto rx_ep_str = remote_udp_host2.ToString();
            ++data_in_count;
            //std::string data_in_str(reinterpret_cast<char const*>(recv_buffer), bytes_recv);
            //printf("UDP recv: %s \n", data_in_str.c_str());
        }

        auto ms_since_last_print = std::chrono::duration_cast<milliseconds>(Clock::now() - msg_count_print_timer);
        if (ms_since_last_print.count() >= REPORT_TEST_DATA_INTERVAL_MILLI)
        {
            msg_count_print_timer = Clock::now();
            printf("MulticastTest in:%llu out:%llu\n", data_in_count, data_out_count);
        }

      //  const int MILLISECS_TO_SLEEP = 1;
      //  Sleep(MILLISECS_TO_SLEEP);
    }
}

void TcpListenAndConnectTest()
{
    TcpSocket tcp_socket_test;
    TcpListenSocket listen_socket;
    //tcp_socket_test.EnableBlocking(false); //TODO: break this test out int diff thread blocking/nonblocking...
    //tcp_socket_test.SetSendBufferSize(11000);
    //tcp_socket_test.SetRecvBufferSize(11000);

    int conn_state = 0;
    auto last_time = Clock::now();
    uint8_t recv_buffer[1024];

    std::string data_to_send("hello from windows world");
    std::string expected_data("hello from linux world");
    auto msg_count_print_timer = Clock::now();
    uint64_t data_in_count = 0;
    uint64_t data_out_count = 0;

    SocketStatus last_socket_status = SocketStatus::Error;

    while (running)
    {
        switch (conn_state)
        {
        case 0:
            ++conn_state;
            last_socket_status = SocketStatus::Error;
            break;

        case 1:
        {
            last_socket_status = socket_utils::listen_and_connect_single_client(listen_socket,
                tcp_listen_ep, tcp_socket_test, 1000);

            if (last_socket_status == SocketStatus::Done)
            {
                printf("connected \n");
                ++conn_state;
            }
            else if (last_socket_status == SocketStatus::Error)
            {
                tcp_socket_test.Disconnect();
                conn_state = 0;
            }
            break;
        }
        case 2:
            milliseconds ms = std::chrono::duration_cast<milliseconds>(Clock::now() - last_time);

            size_t bytes_recv = 0;
            last_socket_status = tcp_socket_test.Receive(recv_buffer, expected_data.length(), bytes_recv);
            //auto send_status = tcp_socket_test.Send((void*)data_to_send.c_str(), data_to_send.length());
            //printf("TCP sending string:: %s %d dt:%d\n", data_to_send.c_str(), ++sent_count, ms.count());
            if (last_socket_status == SocketStatus::Done)
            {
                ++data_in_count;

                //// reply back with other data right away!
                size_t bytes_sent = 0;
                auto send_status = tcp_socket_test.Send((void*)data_to_send.c_str(), data_to_send.length(), bytes_sent);

                if (send_status == SocketStatus::Done)
                    ++data_out_count;

                if (send_status == SocketStatus::Partial)
                {
                    ++data_out_count;
                    printf("send partial %d/%d \n", (int)bytes_sent, (int)data_to_send.length());
                }

                if (send_status == SocketStatus::NotReady)
                    printf("send socket not ready \n");

                if (send_status == SocketStatus::Disconnected)
                {
                    printf("send clean disconnect \n");
                    conn_state = 0;
                }

                if (send_status == SocketStatus::Error)
                {
                    printf("send error disconnect \n");
                    tcp_socket_test.Disconnect();
                    conn_state = 0;
                }
            }

            if (last_socket_status == SocketStatus::Partial)
            {
                ++data_in_count;
                printf("recv partial %d/%d \n", (int)bytes_recv, (int)expected_data.length());
            }

            if (last_socket_status == SocketStatus::NotReady)
                printf("recv socket not ready \n");

            if (last_socket_status == SocketStatus::Disconnected)
            {
                printf("recv clean disconnect \n");
                printf("err: %s \n", socket_utils::error_code_to_descriptive_string(tcp_socket_test.LastSocketErrorCode()).c_str());
                conn_state = 0;
            }

            if (last_socket_status == SocketStatus::Error)
            {
                printf("recv error disconnect \n");
                printf("err: %s \n", socket_utils::error_code_to_descriptive_string(tcp_socket_test.LastSocketErrorCode()).c_str());
                tcp_socket_test.Disconnect();
                  //TODO: consider adding isdataavailable, just like c# has. this could help break out of incoming data
                  // sooner. 
                conn_state = 0;
            }

            if (tcp_socket_test.IsConnected())
            {
                auto ms_since_last_print = std::chrono::duration_cast<milliseconds>(Clock::now() - msg_count_print_timer);
                if (ms_since_last_print.count() >= REPORT_TEST_DATA_INTERVAL_MILLI)
                {
                    msg_count_print_timer = Clock::now();
                    printf("TcpLC in:%llu out:%llu\n", data_in_count, data_out_count);
                }
            }
            break;
        }

        if (last_socket_status == SocketStatus::Done)
            continue;

        const int MILLISECS_TO_SLEEP = 1;
        Sleep(MILLISECS_TO_SLEEP);
    }
}

void TcpSendRecvUtilityTest()
{
    IpEndpoint test_connect_ep = IpEndpoint(linux_ip, 6666);
    IpEndpoint test_listen_ep = IpEndpoint(windows_ip, 6666);
    TcpSocket tcp_socket_test;
    TcpListenSocket listen_socket;
    tcp_socket_test.EnableBlocking(false); //TODO: break this test out int diff thread blocking/nonblocking...

    int conn_state = 0;
    auto last_time = Clock::now();
    auto msg_count_print_timer = Clock::now();
    uint64_t data_in_count = 0;
    uint64_t data_out_count = 0;

    constexpr uint32_t message_data_size = 1048 * 12;

    std::unique_ptr<uint8_t[]> recv_buffer(new uint8_t[message_data_size]);
    std::unique_ptr<uint8_t[]> data_to_send(new uint8_t[message_data_size]);
    memset(data_to_send.get(), 7, message_data_size);

    SocketStatus last_socket_status = SocketStatus::Error;

    std::thread send_thread = std::thread([&]()
    {
        while (running)
        {
            if (!tcp_socket_test.IsConnected())
            {
                Sleep(100);
                continue;
            }

            // reply back with other data right away!
            // right now, trying to get good timeout value on SEND to survive the high throughput...
            size_t bytes_sent = 0;
            auto send_result = socket_utils::try_send_all(tcp_socket_test,
                (uint8_t*)data_to_send.get(), message_data_size, bytes_sent, 3, 20);

            auto send_status = send_result.first;
            auto retry_count = send_result.second;

            if (retry_count > 0)
                printf("send retry cnt %d \n", retry_count);

            if (send_status == SocketStatus::Done)
            {
                ++data_out_count;
                //if (data_out_count % 5000 == 0)
                //    tcp_socket_test.Disconnect();
            }

            if (send_status == SocketStatus::Partial)
            {
                ++data_out_count;
                 printf("send partial %d/%d \n", (int)bytes_sent, (int)message_data_size);
            }

            if (send_status == SocketStatus::NotReady)
                printf("send socket not ready \n");

            if (send_status == SocketStatus::Disconnected ||
                send_status == SocketStatus::Error)
            {
                if (send_status == SocketStatus::Error)
                    printf("send error disconnect\n");

                printf("send disconn code: %s \n", 
                    // when using a util send/recv, can no longer use the cached error code from the socket class.
                    socket_utils::error_code_to_descriptive_string(
                        //socket_impl::last_system_error_code()).c_str()
                        tcp_socket_test.LastSocketErrorCode()).c_str()
                );
                tcp_socket_test.Disconnect();
                conn_state = 0;
            }

            std::this_thread::sleep_for(std::chrono::nanoseconds(1100));
        } 
    });

    while (running)
    {
        switch (conn_state)
        {
        case 0:
            ++conn_state;
            last_socket_status = SocketStatus::Error;
            break;

        case 1:
        {
            last_socket_status = socket_utils::listen_and_connect_single_client(listen_socket,
                test_listen_ep, tcp_socket_test, 1000);
            //last_socket_status = tcp_socket_test.Connect(test_connect_ep, 1000);

            if (last_socket_status == SocketStatus::Done)
            {
                printf("connected \n");
                ++conn_state;
            }
            else if (last_socket_status == SocketStatus::Error)
            {
                printf("connect err: %s \n", 
                    socket_utils::error_code_to_descriptive_string(tcp_socket_test.LastSocketErrorCode()).c_str());
                tcp_socket_test.Disconnect();
                conn_state = 0;
                data_in_count = 0;
                data_out_count = 0;
                Sleep(1000);  // just giving some time after an error...not really needed if using Connect with a timeout value.
            }
            else
            {
                printf("connect dummy print... \n");
            }
            break;
        }
        case 2:
            milliseconds ms = std::chrono::duration_cast<milliseconds>(Clock::now() - last_time);

            size_t bytes_recv = 0;
            last_socket_status = socket_utils::tcp_recv_with_timeout(tcp_socket_test,
                recv_buffer.get(), message_data_size, bytes_recv, 1000);

            if (last_socket_status == SocketStatus::Done)
            {
                ++data_in_count;
            }

            if (last_socket_status == SocketStatus::Partial)
            {
                ++data_in_count;
                // printf("recv partial %d/%d \n", (int)bytes_recv, (int)message_data_size);
            }

            if (last_socket_status == SocketStatus::NotReady)
                printf("recv socket not ready \n");

            if (last_socket_status == SocketStatus::Disconnected ||
                last_socket_status == SocketStatus::Error)
            {
                if (last_socket_status == SocketStatus::Error)
                    printf("recv error disconnect\n");

                printf("recv disconn code: %s \n",
                    // it looks like when a disconnect happens, select() returns an error
                    // wsa error seems to just be the last thing the socket did...
                    socket_utils::error_code_to_descriptive_string(
                        //socket_impl::last_system_error_code()).c_str()
                        tcp_socket_test.LastSocketErrorCode()).c_str()
                );

                tcp_socket_test.Disconnect();
                conn_state = 0;
            }

            break;
        }

        if (tcp_socket_test.IsConnected())
        {
            auto ms_since_last_print = std::chrono::duration_cast<milliseconds>(Clock::now() - msg_count_print_timer);
            if (ms_since_last_print.count() >= REPORT_TEST_DATA_INTERVAL_MILLI)
            {
                msg_count_print_timer = Clock::now();
                //printf("TcpLC in:%llu out:%llu\n", data_in_count, data_out_count);
            }
        }

        // don't sleep if connected
        // with non-blocking configured, this will test tcp_recv_with_timeout performance.
        if (tcp_socket_test.IsConnected())
            continue;
   
        const int MILLISECS_TO_SLEEP = 1;
        Sleep(MILLISECS_TO_SLEEP);
    }

    if (send_thread.joinable())
        send_thread.join();
}

void UdpTestMultipleConnectOnSameSocket()
{
    IpEndpoint linux_udp_conn_ep = IpEndpoint(linux_ip, 2021);
    IpEndpoint linux_udp_conn_ep2 = IpEndpoint(linux_ip, 2022);
    IpEndpoint another_ep = IpEndpoint(linux_ip, 2024);
    std::string test1_data("Test 1:");
    std::string test2_data("Test 2:");
    std::string test3_data("Test 3:");
    std::string test4_data("Test 4:");

    UdpSocket udp_socket;
    udp_socket.Bind({ IpAddress::IP_ANY, 2020 });

    while (running)
    {
        auto conn_res = udp_socket.Connect(linux_udp_conn_ep);
        auto test1 = udp_socket.Send(test1_data.c_str(), test1_data.length());

        auto conn_res2 = udp_socket.Connect(linux_udp_conn_ep2);
        auto test2 = udp_socket.Send(test2_data.c_str(), test2_data.length());

        auto conn_res3 = udp_socket.Disconnect();
        auto test3 = udp_socket.Send(test3_data.c_str(), test3_data.length());   // expect an error here.
        auto test4 = udp_socket.Send(test4_data.c_str(), test4_data.length(), another_ep);

        const int MILLISECS_TO_SLEEP = 1000;
        Sleep(MILLISECS_TO_SLEEP);
    }
}



void JoinThread(std::thread &thread)
{
    if (thread.joinable())
        thread.join();
}

int main()
{
    printf("hello from JetNetworkWindows!\n");

    auto x = sizeof(UdpSocket);
    auto y = sizeof(TcpSocket);
    auto yy = sizeof(TcpListenSocket);
    auto z = sizeof(IpAddress);
    auto zz = sizeof(IpEndpoint);
    auto xxxx = sizeof(sockaddr_in);

    TestSocketUtils();

    //std::thread udp_test_thread(UdpEchoTest);
    //std::thread tcp_test_thread(TcpListenAndConnectTest);
   //  std::thread mcast_test_thread(MulticastTest);
   // std::thread tcp_utils_test_thread(TcpSendRecvUtilityTest);
    std::thread udp_connect_test(UdpTestMultipleConnectOnSameSocket);
    
    auto test_adapters = socket_utils::get_network_ipv4_adapters();

    // wait until console data is entered, then kill the program
    // (text + enter key)
    std::string some_data;
    std::cin >> some_data;

    running = false;

    //JoinThread(udp_test_thread);
    //JoinThread(tcp_test_thread);
    //JoinThread(mcast_test_thread);
   // JoinThread(tcp_utils_test_thread);
    JoinThread(udp_connect_test);
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
