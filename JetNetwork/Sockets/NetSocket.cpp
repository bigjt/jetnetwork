#include "NetSocket.hpp"

NetSocket::NetSocket()
{
}

NetSocket::~NetSocket()
{
    CloseSocket();
}

sockaddr_in NetSocket::CreateSockAddress(uint32_t address, uint16_t port)
{
    sockaddr_in addr{ 0 };
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = htonl(address);
    return addr;
}

SocketStatus NetSocket::EnableReuseAddress(bool enable)
{
    if (socket_handle != socket_impl::INVALID_SOCKET_HANDLE)
    {
        int enable_sock_op = (enable) ? 1 : 0;
        if (setsockopt(socket_handle, SOL_SOCKET, SO_REUSEADDR, (char*)&enable_sock_op, sizeof(enable_sock_op)) != 0)
            return LastErrorAsSocketStatus();
    }

    reuse_addr = enable;
    return SocketStatus::Done;
}

SocketStatus NetSocket::EnableBlocking(bool blocking)
{
    if (socket_handle != socket_impl::INVALID_SOCKET_HANDLE)
    {
        if (socket_impl::enable_blocking(socket_handle, blocking) != SocketStatus::Done)
            return LastErrorAsSocketStatus();
    }

    is_blocking = blocking;
    return SocketStatus::Done;
}

SocketStatus NetSocket::SetSendBufferSize(uint32_t size)
{
    // passing a size of 0 will skip the socketopt and use OS defaults.
    if (socket_handle != socket_impl::INVALID_SOCKET_HANDLE && size != 0)
    {
        if (setsockopt(socket_handle, SOL_SOCKET, SO_SNDBUF, (char*)&size, sizeof(size)) != 0)
            return LastErrorAsSocketStatus();
    }

    send_buffer_size = size;
    return SocketStatus::Done;
}

SocketStatus NetSocket::SetRecvBufferSize(uint32_t size)
{
    // passing a size of 0 will skip the socketopt and use OS defaults.
    if (socket_handle != socket_impl::INVALID_SOCKET_HANDLE && size != 0)
    {
        if (setsockopt(socket_handle, SOL_SOCKET, SO_RCVBUF, (char*)&size, sizeof(size)) != 0)
            return LastErrorAsSocketStatus();
    }

    recv_buffer_size = size;
    return SocketStatus::Done;
}

void NetSocket::CloseSocket()
{
    if (socket_handle == socket_impl::INVALID_SOCKET_HANDLE)
        return;

    socket_impl::close_socket(socket_handle);
    socket_handle = socket_impl::INVALID_SOCKET_HANDLE;
}

SocketStatus NetSocket::LastErrorAsSocketStatus()
{
    last_sys_error_code = socket_impl::last_system_error_code();
    return socket_impl::error_code_as_status(last_sys_error_code);
}

int32_t NetSocket::LastSocketErrorCode()
{
    return last_sys_error_code;
}
