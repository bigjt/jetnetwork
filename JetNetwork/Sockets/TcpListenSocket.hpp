#pragma once
#include "NetSocket.hpp"
#include "../IpEndpoint.hpp"

class TcpSocket;
class TcpListenSocket : public NetSocket
{
public:
    TcpListenSocket() = default;
    ~TcpListenSocket() = default;

    // Provide a local port/address to listen on.
    SocketStatus Listen(const IpEndpoint& listen_endpoint);

    // Accept incoming connection and assign it to the given TcpSocket.
    SocketStatus Accept(TcpSocket& socket);

    // Accept incoming connection and assign it to the given TcpSocket.
    // If the timeout is non-zero, the call will block up to the specified time, even if no connections are accepted.
    // This also applies to non-blocking sockets.
    // Generally, listen sockets should be non-blocking to avoid 
    // the possibility that accept() blocks after select().
    SocketStatus Accept(TcpSocket& socket, uint32_t timeout_milli);

    void Close();

    inline const IpEndpoint& LocalEndpoint() const { return local_endpoint; }
    inline bool IsListening() const { return is_listening; }

private:
    SocketStatus CreateSocket();
    SocketStatus SetupSocket();

    IpEndpoint local_endpoint;
    bool is_listening = false;
};
