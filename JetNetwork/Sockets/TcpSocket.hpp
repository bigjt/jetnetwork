#pragma once
#include "NetSocket.hpp"
#include "../IpEndpoint.hpp"
#include "TcpListenSocket.hpp"

class TcpSocket : public NetSocket
{
public:
    TcpSocket() = default;
    ~TcpSocket() = default;

    // Connect to a remote_endpoint.
    // If the connection timeout is non-zero, the call will block up to the specified time.
    //   This is useful in cases were we want to give up the connection attempt after a certain period.
    // A non-blocking socket will use the timeout too, and will block up to the specified time.
    SocketStatus Connect(const IpEndpoint& remote_endpoint, uint32_t connect_timeout_milli = 0);
    // Connect that binds to a specific local endpoint before connecting.
    SocketStatus Connect(const IpEndpoint& local_endpoint, const IpEndpoint& remote_endpoint, uint32_t connect_timeout_milli = 0);

    void Disconnect();

    // If used with non-blocking socket, caller needs to track the bytes sent.
    SocketStatus Send(uint8_t* data, std::size_t data_size, std::size_t& bytes_sent);

    // If used with non-blocking socket, caller needs to track the bytes received.
    SocketStatus Receive(uint8_t* data, std::size_t data_size, std::size_t& bytes_received);

    // Sets SO_LINGER struct values to either enable a graceful or abortive disconnect.
    //  If false is provided to this function, calling Disconnect() will perform an abortive close with the connected peer.
    // Generally a graceful shutdown is desired, but there may be cases where SO_LINGER needs to be changed 
    //  to deal with reconnect issues, not covered by SO_REUSEADDR.
    //  Example: Previous connections are sitting in the TIME_WAIT because the process was closed unexpectedly.
    //           Enabling abortive close will skip TIME_WAIT. The caller could disable the graceful disconnect by default, 
    //           and re-enable it right before a proper shutdown of the socket (calling Disconnect()). 
    //  Abortive close could also be used to signal to the peer that something went wrong with the connection (ECONNRESET).
    SocketStatus EnableGracefulDisconnect(bool enable);

    inline bool IsConnected() const { return is_connected; }
    inline const IpEndpoint& LocalEndpoint() const { return local_endpoint; }
    inline const IpEndpoint& RemoteEndpoint() const { return remote_endpoint; }

private:
    SocketStatus CreateSocket();
    SocketStatus SetupSocket();
    SocketStatus InternalConnect(const IpAddress& remote_ip, uint16_t remote_port, uint32_t connect_timeout_milli);

    IpEndpoint local_endpoint;
    IpEndpoint remote_endpoint;
    bool is_connected = false;
    bool enable_graceful_disconnect = true;

    friend class TcpListenSocket;

public:
    template <typename T>
    SocketStatus Send(T data, std::size_t data_size, std::size_t& bytes_sent);
    template <typename T>
    SocketStatus Receive(T data, std::size_t data_size, std::size_t& bytes_received);
};

template <typename T>
SocketStatus TcpSocket::Send(T data, std::size_t data_size, std::size_t& bytes_sent)
{
    static_assert(std::is_pointer<T>::value, "TcpSocket::Send - data should be a pointer type");
    return Send((uint8_t*)data, data_size, bytes_sent);
}

template <typename T>
SocketStatus TcpSocket::Receive(T data, std::size_t data_size, std::size_t& bytes_received)
{
    static_assert(std::is_pointer<T>::value, "TcpSocket::Receive - data should be a pointer type");
    return Receive((uint8_t*)data, data_size, bytes_received);
}
