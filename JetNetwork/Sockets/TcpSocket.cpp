#include "TcpSocket.hpp"
#include "../SocketUtilsImpl.hpp"

SocketStatus TcpSocket::CreateSocket()
{
    if (socket_handle != socket_impl::INVALID_SOCKET_HANDLE)
        return SocketStatus::Done;

    last_sys_error_code = 0;

    socket_handle = socket(PF_INET, SOCK_STREAM, 0);
    if (socket_handle == socket_impl::INVALID_SOCKET_HANDLE)
        return LastErrorAsSocketStatus();

    auto socket_status = SetupSocket();
    if (socket_status != SocketStatus::Done)
        CloseSocket(); // clears socket_handle

    return socket_status;
}

// If changing socket settings here, also look at changing TcpListenSocket::SetupSocket.
SocketStatus TcpSocket::SetupSocket()
{
    if (socket_handle == socket_impl::INVALID_SOCKET_HANDLE)
        return SocketStatus::Error;

    SocketStatus sock_opt_status = EnableBlocking(IsBlocking());
    if (sock_opt_status == SocketStatus::Error)
        return sock_opt_status;

    sock_opt_status = EnableReuseAddress(IsReusingAddress());
    if (sock_opt_status == SocketStatus::Error)
        return sock_opt_status;

    sock_opt_status = SetSendBufferSize(SendBufferSize());
    if (sock_opt_status == SocketStatus::Error)
        return sock_opt_status;

    sock_opt_status = SetRecvBufferSize(RecvBufferSize());
    if (sock_opt_status == SocketStatus::Error)
        return sock_opt_status;

    sock_opt_status = EnableGracefulDisconnect(enable_graceful_disconnect);
    if (sock_opt_status == SocketStatus::Error)
        return sock_opt_status;

    int enable_sock_op = 1;
    setsockopt(socket_handle, IPPROTO_TCP, TCP_NODELAY, (char*)&enable_sock_op, sizeof(enable_sock_op));

    
    return SocketStatus::Done;
}

SocketStatus TcpSocket::Connect(const IpEndpoint& remote_endpoint, uint32_t connect_timeout_milli)
{
    if (is_connected)
    {
        return (remote_endpoint == this->remote_endpoint) 
            ? SocketStatus::Done 
            : SocketStatus::Error;
    }

    const auto& ip = remote_endpoint.IpAddr();
    if (!ip.IsValid() || ip.IsBroadcast() || ip.IsMulticast() || ip.IsAny())
        return SocketStatus::Error;

    const auto create_socket_status = CreateSocket();
    if (create_socket_status != SocketStatus::Done)
        return create_socket_status;

    return InternalConnect(ip, remote_endpoint.Port(), connect_timeout_milli);
}

SocketStatus TcpSocket::Connect(const IpEndpoint& local_endpoint, const IpEndpoint& remote_endpoint, uint32_t connect_timeout_milli)
{
    if (is_connected)
    {
        return (local_endpoint == this->local_endpoint && remote_endpoint == this->remote_endpoint)
            ? SocketStatus::Done
            : SocketStatus::Error;
    }

    const auto& local_ip = local_endpoint.IpAddr();
    if (!local_ip.IsValid() || local_ip.IsBroadcast() || local_ip.IsMulticast())
        return SocketStatus::Error;

    const auto& remote_ip = remote_endpoint.IpAddr();
    if (!remote_ip.IsValid() || remote_ip.IsBroadcast() || remote_ip.IsMulticast() || remote_ip.IsAny())
        return SocketStatus::Error;

    const auto create_socket_status = CreateSocket();
    if (create_socket_status != SocketStatus::Done)
        return create_socket_status;

    const sockaddr_in address = CreateSockAddress(local_ip.HostOrderAddress(), local_endpoint.Port());
    if (bind(socket_handle, (sockaddr*)&address, sizeof(address)) == -1)
    {
        CloseSocket();
        return LastErrorAsSocketStatus();
    }

    return InternalConnect(remote_ip, remote_endpoint.Port(), connect_timeout_milli);
}

SocketStatus TcpSocket::InternalConnect(const IpAddress& remote_ip, uint16_t remote_port, uint32_t connect_timeout_milli)
{
    const sockaddr_in address = CreateSockAddress(remote_ip.HostOrderAddress(), remote_port);

    if (connect_timeout_milli == 0)
    {
        if (connect(socket_handle, (sockaddr*)&address, sizeof(address)) == -1)
            return LastErrorAsSocketStatus();
    }
    else
    {
        bool configured_as_blocking = IsBlocking();
        SocketStatus connect_status = SocketStatus::Error;

        // Switch to non-blocking to enable the connection timeout
        if (configured_as_blocking)
            EnableBlocking(false);

        // Try to connect to the remote address
        if (connect(socket_handle, (sockaddr*)(&address), sizeof(address)) >= 0)
        {
            // connect worked right away!
            connect_status = SocketStatus::Done;
        }
        else
        {
            // Connect didn't work, check for errors
            // and enter the timeout logic.

            // check for problems with the socket
            connect_status = LastErrorAsSocketStatus();

            // If there are no problems, setup the timeout to check the state of the connection.
            if (connect_status == SocketStatus::NotReady)
            {
                SocketStatus wait_status = socket_utils::socket_wait_for_send_ready(*this, connect_timeout_milli);

                // If the socket is ready to send, then we connected!
                if (wait_status == SocketStatus::Done)
                {
                    // To know whether the connection is good, try and get the remote address
                    socklen_t addr_size = sizeof(address);
                    if (getpeername(socket_handle, (struct sockaddr*)&address, &addr_size) != -1)
                    {
                        // Connection accepted
                        connect_status = SocketStatus::Done;
                    }
                    else
                    {
                        // Connection refused
                        connect_status = LastErrorAsSocketStatus();
                    }
                }
                else
                {
                    // No connection
                    connect_status = (wait_status == SocketStatus::NotReady)
                        ? SocketStatus::NotReady     // Timeout reached
                        : LastErrorAsSocketStatus(); // Wait error
                }
            }
        }

        // Switch back to blocking mode
        if (configured_as_blocking)
            EnableBlocking(true);

        if (connect_status != SocketStatus::Done)
            return connect_status;
    }

    // capture the local ip/port of the connection
    sockaddr_in local_sock_addr;
    socklen_t local_addr_size = sizeof(local_sock_addr);
    if (getsockname(socket_handle, (struct sockaddr*)&local_sock_addr, &local_addr_size) == -1)
    {
        return LastErrorAsSocketStatus();
    }

    local_endpoint = IpEndpoint(local_sock_addr);
    this->remote_endpoint = IpEndpoint(address);
    is_connected = true;
    return SocketStatus::Done;
}

void TcpSocket::Disconnect()
{
    // this check was added for windows (SO_LINGER settings are ignored
    //  when shutdown is called). BUT is OK for other impl as well.
    if (enable_graceful_disconnect)
        socket_impl::shutdown_socket(socket_handle);

    CloseSocket();
    is_connected = false;
    local_endpoint = IpEndpoint(IpAddress::IP_NONE, AnyPort);
    remote_endpoint = IpEndpoint(IpAddress::IP_NONE, AnyPort);
}

SocketStatus TcpSocket::Send(uint8_t* data, std::size_t data_size, std::size_t& bytes_sent)
{
    bytes_sent = 0;

    if (data_size == 0)
        return SocketStatus::Error;

    if (socket_handle == socket_impl::INVALID_SOCKET_HANDLE)
        return SocketStatus::Error;

    do
    {
        auto bytes_sent_now = send(socket_handle,
            ((const char*)data) + bytes_sent, (int)(data_size - bytes_sent), socket_impl::TCP_SEND_RECV_FLAGS);

        if (bytes_sent_now < 0)
        {
            SocketStatus status = LastErrorAsSocketStatus();
            if (status == SocketStatus::NotReady)
            {
                // For non-blocking sockets, being here means the send buffer may be full.
                return (bytes_sent > 0) ? SocketStatus::Partial : SocketStatus::NotReady;
            }

            // The socket is disconnected or errored.
            is_connected = false;
            return status;
        }

        bytes_sent += bytes_sent_now;
    } while (bytes_sent != data_size);

    return SocketStatus::Done;
}

SocketStatus TcpSocket::Receive(uint8_t* data, std::size_t data_size, std::size_t& bytes_received)
{
    bytes_received = 0;

    if (data_size == 0)
        return SocketStatus::Error;

    if (socket_handle == socket_impl::INVALID_SOCKET_HANDLE)
        return SocketStatus::Error;

    auto bytes_recv = recv(socket_handle, (char*)data, (int)data_size, socket_impl::TCP_SEND_RECV_FLAGS);

    if (bytes_recv <= 0)
    {
        if (bytes_recv == 0)
        {
            // clean disconnect
            // clearing the last error to avoid reporting a stale value to caller. 
            last_sys_error_code = 0;
            is_connected = false;
            return SocketStatus::Disconnected;
        }
        else
        {
            auto status = LastErrorAsSocketStatus();
            if (status != SocketStatus::NotReady)
            {
                // dirty disconnect
                is_connected = false;
            }

            return status;
        }
    }

    // good read
    bytes_received = (size_t)bytes_recv;
    return (bytes_received != data_size) ? SocketStatus::Partial : SocketStatus::Done;
}

SocketStatus TcpSocket::EnableGracefulDisconnect(bool enable)
{
    if (socket_handle != socket_impl::INVALID_SOCKET_HANDLE)
    {
        // Graceful == struct linger linger = { 0, 0 }; (FIN sent on close)
        // Abortive == struct linger linger = { 1, 0 }; (RST sent on close)
        struct linger linger;
        linger.l_onoff = (enable) ? 0 : 1;
        linger.l_linger = 0;

        if (setsockopt(socket_handle, SOL_SOCKET, SO_LINGER, (char*)&linger, sizeof(linger)) != 0)
            return LastErrorAsSocketStatus();
    }

    enable_graceful_disconnect = enable;
    return SocketStatus::Done;
}
