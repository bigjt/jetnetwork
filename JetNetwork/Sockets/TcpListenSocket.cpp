#include "TcpListenSocket.hpp"
#include "TcpSocket.hpp" // forward delcared in header
#include "../SocketUtilsImpl.hpp"

SocketStatus TcpListenSocket::CreateSocket()
{
    if (socket_handle != socket_impl::INVALID_SOCKET_HANDLE)
        return SocketStatus::Done;

    last_sys_error_code = 0;
    is_listening = false;

    socket_handle = socket(PF_INET, SOCK_STREAM, 0);
    if (socket_handle == socket_impl::INVALID_SOCKET_HANDLE)
        return LastErrorAsSocketStatus();

    auto status = SetupSocket();
    if (status != SocketStatus::Done)
        CloseSocket(); // clears socket_handle

    return status;
}

SocketStatus TcpListenSocket::SetupSocket()
{
    if (socket_handle == socket_impl::INVALID_SOCKET_HANDLE)
        return SocketStatus::Error;

    SocketStatus sock_opt_status = EnableBlocking(IsBlocking());
    if (sock_opt_status == SocketStatus::Error)
        return sock_opt_status;

    sock_opt_status = EnableReuseAddress(IsReusingAddress());
    if (sock_opt_status == SocketStatus::Error)
        return sock_opt_status;

    sock_opt_status = SetSendBufferSize(SendBufferSize());
    if (sock_opt_status == SocketStatus::Error)
        return sock_opt_status;

    sock_opt_status = SetRecvBufferSize(RecvBufferSize());
    if (sock_opt_status == SocketStatus::Error)
        return sock_opt_status;

    int enable_sock_op = 1;
    setsockopt(socket_handle, IPPROTO_TCP, TCP_NODELAY, (char*)&enable_sock_op, sizeof(enable_sock_op));

    // Turn off LINGER to support a graceful shutdown on close()
    struct linger linger = { 0, 0 }; // l_onoff = 0, l_linger = 0.
    setsockopt(socket_handle, SOL_SOCKET, SO_LINGER, (char*)&linger, sizeof(linger));

    return SocketStatus::Done;
}

SocketStatus TcpListenSocket::Listen(const IpEndpoint& listen_endpoint)
{
    if (is_listening)
    {
        return (listen_endpoint == local_endpoint)
            ? SocketStatus::Done
            : SocketStatus::Error;
    }

    const auto& ip = listen_endpoint.IpAddr();
    if (!ip.IsValid() || ip.IsBroadcast() || ip.IsMulticast())
        return SocketStatus::Error;

    auto socket_status = CreateSocket();
    if (socket_status != SocketStatus::Done)
        return socket_status;

    sockaddr_in address = CreateSockAddress(ip.HostOrderAddress(), listen_endpoint.Port());
    if (bind(socket_handle, (sockaddr*)&address, sizeof(address)) == -1)
    {
        // Failed to bind.
        return LastErrorAsSocketStatus();
    }

    if (listen(socket_handle, SOMAXCONN) == -1)
    {
        // Can't listen on this socket
        return LastErrorAsSocketStatus();
    }

    local_endpoint = listen_endpoint;
    is_listening = true;
    return SocketStatus::Done;
}

SocketStatus TcpListenSocket::Accept(TcpSocket& socket)
{
    if (socket_handle == socket_impl::INVALID_SOCKET_HANDLE)
        return SocketStatus::Error;

    if (!is_listening)
        return SocketStatus::NotReady;

    // Try to accept a new connection
    sockaddr_in address;
    socklen_t addr_size = sizeof(address);
    SocketHandle connection_socket_handle = accept(socket_handle, (sockaddr*)&address, &addr_size);

    if (connection_socket_handle == socket_impl::INVALID_SOCKET_HANDLE)
        return LastErrorAsSocketStatus();

    // Cleanup old state of the socket
    socket.last_sys_error_code = 0;
    socket.CloseSocket();

    // Setup connection socket
    socket.socket_handle = connection_socket_handle;
    auto setup_status = socket.SetupSocket();
    if (setup_status != SocketStatus::Done)
    {
        // Make accepted socket error code visible through this class
        last_sys_error_code = socket.LastSocketErrorCode();
        socket.CloseSocket();
        return setup_status;
    }

    // Capture the local ip/port of the connection
    sockaddr_in local_sock_addr;
    socklen_t local_addr_size = sizeof(local_sock_addr);
    if (getsockname(connection_socket_handle, (struct sockaddr*)&local_sock_addr, &local_addr_size) == -1)
    {
        // Make accepted socket error code visible through this class
        last_sys_error_code = socket.LastSocketErrorCode();
        socket.CloseSocket();
        return SocketStatus::Error;
    }

    // Copy over socket info to the TcpSocket
    socket.remote_endpoint = IpEndpoint(address);
    socket.local_endpoint = IpEndpoint(local_sock_addr);
    socket.is_connected = true;
    return SocketStatus::Done;
}

SocketStatus TcpListenSocket::Accept(TcpSocket& socket, uint32_t timeout_milli)
{
    if (socket_handle == socket_impl::INVALID_SOCKET_HANDLE)
        return SocketStatus::Error;

    if (!is_listening)
        return SocketStatus::NotReady;

    // Calling accept() before select() because both calls will return immediately.
    if (timeout_milli == 0 && !IsBlocking())
        return Accept(socket);

    // Calling accept() before select() because both calls will block.
    if (timeout_milli == SOCKET_INFINITE_TIMEOUT && IsBlocking())
        return Accept(socket);

    // when a listening socket gets an incoming connection request,
    // select() will notify with a read ready FD.
    SocketStatus accept_status = socket_utils::socket_wait_for_recv_ready(*this, timeout_milli);

    if (accept_status == SocketStatus::Done)
        return Accept(socket);

    return accept_status;
}

void TcpListenSocket::Close()
{
    NetSocket::CloseSocket();
    is_listening = false;
    local_endpoint = IpEndpoint(IpAddress::IP_NONE, AnyPort);
}
