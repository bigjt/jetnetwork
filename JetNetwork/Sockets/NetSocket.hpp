#pragma once
#include "../SocketImpl.hpp"
#include <mutex>
#include <stdint.h>

class NetSocket
{
public:
    SocketStatus EnableBlocking(bool blocking);
    SocketStatus EnableReuseAddress(bool enable);

    // Suggest using the OS default setting for the send buffer size.
    // Only use this function if the data rate requires a larger buffer.
    SocketStatus SetSendBufferSize(uint32_t size);

    // Suggest using the OS default setting for the recv buffer size.
    // Only use this function if the data rate requires a larger buffer.
    SocketStatus SetRecvBufferSize(uint32_t size);

    inline bool IsBlocking() const { return is_blocking; }
    inline bool IsReusingAddress() const { return reuse_addr; }

    // returns 0 if using OS default buffer size
    inline uint32_t SendBufferSize() const { return send_buffer_size; }

    // returns 0 if using OS default buffer size
    inline uint32_t RecvBufferSize() const { return recv_buffer_size; }

    // returns the raw socket descriptor
    inline SocketHandle Handle() const { return socket_handle; }

    // returns the last system error code generated from failed socket operations.
    // Call this after detecting SocketStatus::Error, SocketStatus::Disconnected, or SocketStatus::NotReady to capture more detailed information.
    //  Pulling the error code any other time may result old or unuseful information.
    int32_t LastSocketErrorCode();

protected:
    // Protected constructor to limit use of this class to sockets.
    NetSocket();
    // Non-virtual protected destructor. I don't forsee the need to delete a socket from this base class.
    ~NetSocket();

    void CloseSocket();
    sockaddr_in CreateSockAddress(uint32_t address, uint16_t port);

    // This function is used to cache the last error code after a socket api function
    // fails to complete. Some of the error codes just mean the socket isn't ready yet, which is often the case
    // for non-blocking sockets. This function's purpose is to translate the error codes into the SocketStatus enum
    // so the caller can take the appropriate action.
    // Child classes should use this when returning up errors to the caller.
    SocketStatus LastErrorAsSocketStatus();

    SocketHandle socket_handle = socket_impl::INVALID_SOCKET_HANDLE;
    int32_t last_sys_error_code = 0;

private:
    NetSocket(const NetSocket&) = delete;
    NetSocket& operator=(const NetSocket&) = delete;

    bool is_blocking = false;
    bool reuse_addr = true;
    uint32_t send_buffer_size = 0;
    uint32_t recv_buffer_size = 0;
};
