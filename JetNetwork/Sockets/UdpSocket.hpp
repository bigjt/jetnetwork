#pragma once
#include "NetSocket.hpp"
#include "../IpEndpoint.hpp"

class UdpSocket : public NetSocket
{
public:
    UdpSocket() = default;
    ~UdpSocket() = default;

    // Create the socket and bind to an interface to sendto and recvfrom
    // For multicast: Bind to IP_ANY to send/recv from the system's default multicast interface.
    //    Othewise, bind to a network adapter ip to send/recv using a specific interface.
    SocketStatus Bind(const IpEndpoint& local_endpoint);
    SocketStatus Receive(uint8_t* data, std::size_t data_size, std::size_t& bytes_received, IpEndpoint& remote_endpoint);
    SocketStatus Send(uint8_t* data, size_t data_size, const IpEndpoint& remote_endpoint);
    // closes the socket, leaves any joined multicast groups
    void Unbind();

    // Connect() will establish a default remote endpoint. 
    // This will limit send and receive to a single endpoint. Packets that arrive from an address 
    // other than the default remote endpoint will be discarded.
    // Caller can only use Send(uint8_t*, size_t) after successful Connect or the socket will report an error.
    // A socket connected to a multicast address may only be used to send packets.
    // Use Disconnect to clear the default remote endpoint. Caller can swap connected endpoints by calling Connect again.
    SocketStatus Connect(const IpEndpoint& remote_endpoint);
    SocketStatus Disconnect();
    SocketStatus Send(uint8_t* data, size_t data_size);

    // Specify router hop limit for outgoing multicast datagrams.
    // See MutlicastTtlThresholds for general specifications.
    SocketStatus SetMulticastTimeToLive(uint8_t ttl);
    SocketStatus SetMulticastTimeToLive(MutlicastTtlThreshold ttl) { return SetMulticastTimeToLive((uint8_t)ttl); }

    // enable or disable the loopback of outgoing multicast datagrams
    SocketStatus EnableMulticastLoopback(bool enable);
    SocketStatus JoinMulticastGroup(const IpAddress& group_address);
    SocketStatus LeaveMulticastGroup(const IpAddress& group_address);

    inline bool IsConnected() const { return is_connected; }
    inline const IpEndpoint& LocalEndpoint() const { return local_endpoint; }
    inline const IpEndpoint& ConnectedRemoteEndpoint() const { return connected_remote_endpoint; }
    inline bool IsMulticastLoopbackEnabled() const { return is_mcast_loopback_enabled; }
    inline uint8_t MulticastTimeToLive() const { return mcast_time_to_live; }

protected:
    // enabling broadcast by default, promote to public if desired.
    SocketStatus EnableBroadcast(bool enable);
    inline bool IsBroadcastEnabled() const { return is_broadcast_enabled; }

private:
    SocketStatus CreateSocket();
    SocketStatus SetupSocket();

    IpEndpoint local_endpoint;
    IpEndpoint connected_remote_endpoint;
    bool is_bound = false;
    bool is_broadcast_enabled = true;
    bool is_connected = false;
    bool is_mcast_loopback_enabled = false;
    uint8_t mcast_time_to_live = (uint8_t)MutlicastTtlThreshold::SameSite;

public:
    template <typename T>
    SocketStatus Send(T data, size_t data_size, const IpEndpoint& remote_endpoint);
    template <typename T>
    SocketStatus Send(T data, size_t data_size);
    template <typename T>
    SocketStatus Receive(T data, std::size_t data_size, std::size_t& bytes_received, IpEndpoint& remote_endpoint);
};

template <typename T>
SocketStatus UdpSocket::Send(T data, size_t data_size, const IpEndpoint& remote_endpoint)
{
    static_assert(std::is_pointer<T>::value, "UdpSocket::Send - data should be a pointer type");
    return Send((uint8_t*)data, data_size, remote_endpoint);
}

template <typename T>
SocketStatus UdpSocket::Send(T data, size_t data_size)
{
    static_assert(std::is_pointer<T>::value, "UdpSocket::Send - data should be a pointer type");
    return Send((uint8_t*)data, data_size);
}

template <typename T>
SocketStatus UdpSocket::Receive(T data, std::size_t data_size, std::size_t& bytes_received, IpEndpoint& remote_endpoint)
{
    static_assert(std::is_pointer<T>::value, "UdpSocket::Receive - data should be a pointer type");
    return Receive((uint8_t*)data, data_size, bytes_received, remote_endpoint);
}
