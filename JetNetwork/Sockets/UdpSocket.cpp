#include "UdpSocket.hpp"

namespace
{
SocketStatus change_group_membership(SocketHandle socket, int add_or_drop, const IpAddress& group_address, const IpAddress& adapter_interface_ip)
{
    struct ip_mreq mreq;
    mreq.imr_multiaddr.s_addr = htonl(group_address.HostOrderAddress());
    mreq.imr_interface.s_addr = htonl(adapter_interface_ip.HostOrderAddress());

    if (setsockopt(socket, IPPROTO_IP, add_or_drop, (char*)& mreq, sizeof(mreq)) != 0)
        return SocketStatus::Error;

    return SocketStatus::Done;
}

SocketStatus disconnect_socket(SocketHandle socket)
{
    if (socket == socket_impl::INVALID_SOCKET_HANDLE)
        return SocketStatus::Error;

    // If the sa_family member of address is AF_UNSPEC, the socket's peer address shall be reset.
    sockaddr_in empty_address{};
    empty_address.sin_family = AF_UNSPEC;

    if (connect(socket, (sockaddr*)&empty_address, sizeof(empty_address)) == -1)
        return SocketStatus::Error;

    return SocketStatus::Done;
}

} // namespace

SocketStatus UdpSocket::CreateSocket()
{
    if (socket_handle != socket_impl::INVALID_SOCKET_HANDLE)
        return SocketStatus::Done;

    last_sys_error_code = 0;

    socket_handle = socket(PF_INET, SOCK_DGRAM, 0);
    if (socket_handle == socket_impl::INVALID_SOCKET_HANDLE)
        return LastErrorAsSocketStatus();

    auto socket_status = SetupSocket();
    if (socket_status != SocketStatus::Done)
        CloseSocket(); // clears socket_handle

    return socket_status;
}

SocketStatus UdpSocket::SetupSocket()
{
    if (socket_handle == socket_impl::INVALID_SOCKET_HANDLE)
        return SocketStatus::Error;

    SocketStatus sock_opt_status = EnableBlocking(IsBlocking());
    if (sock_opt_status == SocketStatus::Error)
        return sock_opt_status;

    sock_opt_status = EnableReuseAddress(IsReusingAddress());
    if (sock_opt_status == SocketStatus::Error)
        return sock_opt_status;

    sock_opt_status = SetSendBufferSize(SendBufferSize());
    if (sock_opt_status == SocketStatus::Error)
        return sock_opt_status;

    sock_opt_status = SetRecvBufferSize(RecvBufferSize());
    if (sock_opt_status == SocketStatus::Error)
        return sock_opt_status;

    sock_opt_status = EnableBroadcast(is_broadcast_enabled);
    if (sock_opt_status == SocketStatus::Error)
        return sock_opt_status;

    sock_opt_status = SetMulticastTimeToLive(mcast_time_to_live);
    if (sock_opt_status == SocketStatus::Error)
        return sock_opt_status;

    sock_opt_status = EnableMulticastLoopback(is_mcast_loopback_enabled);
    if (sock_opt_status == SocketStatus::Error)
        return sock_opt_status;

    return SocketStatus::Done;
}

SocketStatus UdpSocket::Bind(const IpEndpoint& local_endpoint)
{
    if (is_bound)
    {
        return (local_endpoint == this->local_endpoint)
            ? SocketStatus::Done
            : SocketStatus::Error;
    }

    auto socket_status = CreateSocket();
    if (socket_status != SocketStatus::Done)
        return socket_status;

    const auto& local_ip = local_endpoint.IpAddr();
    if (!local_ip.IsValid() || local_ip.IsBroadcast() || local_ip.IsMulticast())
        return SocketStatus::Error;

    const sockaddr_in address = CreateSockAddress(local_ip.HostOrderAddress(), local_endpoint.Port());
    if (bind(socket_handle, (sockaddr*)&address, sizeof(address)) == -1)
    {
        CloseSocket();
        return LastErrorAsSocketStatus();
    }

#ifdef WIN32
    // It is OK to turn this feature off -- UDP comms should be treated as unreliable.
    socket_impl::enable_udp_icmp_recv_error(socket_handle, false);
#endif

    this->local_endpoint = local_endpoint;
    is_bound = true;
    return SocketStatus::Done;
}

void UdpSocket::Unbind()
{
    CloseSocket();
    is_bound = false;
    is_connected = false;
    local_endpoint = IpEndpoint(IpAddress::IP_NONE, AnyPort);
    connected_remote_endpoint = IpEndpoint(IpAddress::IP_NONE, AnyPort);
}

SocketStatus UdpSocket::Receive(uint8_t* data, std::size_t data_size, std::size_t& bytes_received, IpEndpoint& remote_endpoint)
{
    bytes_received = 0;
    remote_endpoint = IpEndpoint(IpAddress::IP_NONE, AnyPort);

    if (socket_handle == socket_impl::INVALID_SOCKET_HANDLE)
        return SocketStatus::Error;

    sockaddr_in source_addr{ 0 };
    socklen_t addr_size = sizeof(source_addr);
    auto bytes_recv = recvfrom(socket_handle, (char*)data, (int)data_size, 0, (sockaddr*)&source_addr, &addr_size);

    if (bytes_recv < 0)
        return LastErrorAsSocketStatus();

    bytes_received = (size_t)bytes_recv;
    remote_endpoint = IpEndpoint(IpAddress(ntohl(source_addr.sin_addr.s_addr)), ntohs(source_addr.sin_port));
    
    // UDP supports payloadless packets (size == 0), return Done even if size is 0.
    return SocketStatus::Done;
}

SocketStatus UdpSocket::Send(uint8_t* data, size_t data_size, const IpEndpoint& remote_endpoint)
{
    if (socket_handle == socket_impl::INVALID_SOCKET_HANDLE)
        return SocketStatus::Error;

    // connected UDP sockets cannot call sendto with a valid remote_endpoint.
    if (is_connected)
        return SocketStatus::Error;

    if (data_size > MAX_UDP_PACKET_SIZE)
        return SocketStatus::Error;

    const auto& remote_ip = remote_endpoint.IpAddr();
    if (!remote_ip.IsValid() || remote_ip.IsAny())
        return SocketStatus::Error;

    sockaddr_in address = CreateSockAddress(remote_ip.HostOrderAddress(), remote_endpoint.Port());
    auto bytes_sent = sendto(socket_handle, (const char*)data, (int)data_size, 0, (sockaddr*)&address, sizeof(address));

    if (bytes_sent < 0)
        return LastErrorAsSocketStatus();

    return SocketStatus::Done;
}

SocketStatus UdpSocket::Connect(const IpEndpoint& remote_endpoint)
{
    if (socket_handle == socket_impl::INVALID_SOCKET_HANDLE)
        return SocketStatus::Error;

    if (is_connected && remote_endpoint == connected_remote_endpoint)
        return SocketStatus::Done;

    const auto& remote_ip = remote_endpoint.IpAddr();
    if (!remote_ip.IsValid() || remote_ip.IsAny())
        return SocketStatus::Error;

    sockaddr_in address = CreateSockAddress(remote_ip.HostOrderAddress(), remote_endpoint.Port());
    if (connect(socket_handle, (sockaddr*)&address, sizeof(address)) == -1)
    {
        // grab the error before making another socket call to disconnect.
        SocketStatus error_status = LastErrorAsSocketStatus();

        if (is_connected)
        {
            if (disconnect_socket(socket_handle) == SocketStatus::Done)
                is_connected = false;
        }

        return error_status;
    }
     
    connected_remote_endpoint = remote_endpoint;
    is_connected = true;
    return SocketStatus::Done;
}

SocketStatus UdpSocket::Disconnect()
{
    if (socket_handle == socket_impl::INVALID_SOCKET_HANDLE)
        return SocketStatus::Error;

    if (!is_connected)
        return SocketStatus::Done;

    if (disconnect_socket(socket_handle) != SocketStatus::Done)
        return LastErrorAsSocketStatus();

    is_connected = false;
    return SocketStatus::Done;
}

SocketStatus UdpSocket::Send(uint8_t* data, size_t data_size)
{
    if (socket_handle == socket_impl::INVALID_SOCKET_HANDLE)
        return SocketStatus::Error;

    if (!is_connected)
        return SocketStatus::Error;

    if (data_size > MAX_UDP_PACKET_SIZE)
        return SocketStatus::Error;

    auto bytes_sent = send(socket_handle, (const char*)data, (int)data_size, 0);

    if (bytes_sent < 0)
        return LastErrorAsSocketStatus();

    return SocketStatus::Done;
}

SocketStatus UdpSocket::EnableBroadcast(bool enable)
{
    if (socket_handle != socket_impl::INVALID_SOCKET_HANDLE)
    {
        int enable_sock_op = (enable) ? 1 : 0;
        if (setsockopt(socket_handle, SOL_SOCKET, SO_BROADCAST, (char*)& enable_sock_op, sizeof(enable_sock_op)) != 0)
            return LastErrorAsSocketStatus();
    }

    is_broadcast_enabled = enable;
    return SocketStatus::Done;
}

SocketStatus UdpSocket::SetMulticastTimeToLive(uint8_t ttl)
{
    if (socket_handle != socket_impl::INVALID_SOCKET_HANDLE)
    {
        if (setsockopt(socket_handle, IPPROTO_IP, IP_MULTICAST_TTL, (char*)& ttl, sizeof(ttl)) != 0)
            return LastErrorAsSocketStatus();
    }

    mcast_time_to_live = ttl;
    return SocketStatus::Done;
}

SocketStatus UdpSocket::EnableMulticastLoopback(bool enable)
{
    if (socket_handle != socket_impl::INVALID_SOCKET_HANDLE)
    {
        uint8_t enable_loop = enable;
        if (setsockopt(socket_handle, IPPROTO_IP, IP_MULTICAST_LOOP, (char*)& enable_loop, sizeof(enable_loop)) != 0)
            return LastErrorAsSocketStatus();
    }

    is_mcast_loopback_enabled = enable;
    return SocketStatus::Done;
}

SocketStatus UdpSocket::JoinMulticastGroup(const IpAddress& group_address)
{
    if (socket_handle == socket_impl::INVALID_SOCKET_HANDLE)
        return SocketStatus::Error;

    if (!group_address.IsValid() || !group_address.IsMulticast())
        return SocketStatus::Error;

    if (change_group_membership(socket_handle, IP_ADD_MEMBERSHIP, group_address, local_endpoint.IpAddr()) == SocketStatus::Error)
        return LastErrorAsSocketStatus();

    return SocketStatus::Done;
}

SocketStatus UdpSocket::LeaveMulticastGroup(const IpAddress& group_address)
{
    if (socket_handle == socket_impl::INVALID_SOCKET_HANDLE)
        return SocketStatus::Error;

    if (!group_address.IsValid() || !group_address.IsMulticast())
        return SocketStatus::Error;

    if (change_group_membership(socket_handle, IP_DROP_MEMBERSHIP, group_address, local_endpoint.IpAddr()) == SocketStatus::Error)
        return LastErrorAsSocketStatus();

    return SocketStatus::Done;
}
