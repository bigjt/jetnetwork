#include "SocketUtilsImpl.hpp"
#include "SocketImpl.hpp"
#include "IpAddress.hpp"
#include "IpEndpoint.hpp"
#include "Sockets/NetSocket.hpp"
#include "Sockets/TcpSocket.hpp"
#include "Sockets/UdpSocket.hpp"
#include "Sockets/TcpListenSocket.hpp"

#include <atomic>

namespace
{
bool are_ips_in_same_subnet(uint32_t host_order_ip1, uint32_t host_order_ip2, uint32_t host_order_netmask)
{
    if (host_order_netmask == 0)
        return false;

    // put data in network order (big endian) for crossplat compatibility
    uint32_t ip1 = htonl(host_order_ip1);
    uint32_t ip2 = htonl(host_order_ip2);
    uint32_t netmask = htonl(host_order_netmask);

    return ((ip1 & netmask) == (ip2 & netmask));
}

uint32_t get_socket_buf_size(int socket_type, int buf_type)
{
    int32_t size = 0;
    socklen_t sock_opt_size = sizeof(size);
    SocketHandle socket_handle = socket(PF_INET, socket_type, 0);

    if (socket_handle == socket_impl::INVALID_SOCKET_HANDLE)
        return 0;

    // if sockopt fails, just make sure size isn't set to some garbage data.
    if (getsockopt(socket_handle, SOL_SOCKET, buf_type, (char*)&size, &sock_opt_size) != 0)
        size = 0;

    socket_impl::close_socket(socket_handle);
    return (uint32_t)size;
}

} // namespace

namespace socket_utils
{
uint32_t default_udp_sendbuf_size()
{
    static std::atomic<uint32_t> cached_size(0);

    if (cached_size != 0)
        return cached_size;

    cached_size = get_socket_buf_size(SOCK_DGRAM, SO_SNDBUF);
    return cached_size;
}

uint32_t default_udp_recvbuf_size()
{
    static std::atomic<uint32_t> cached_size(0);

    if (cached_size != 0)
        return cached_size;

    cached_size = get_socket_buf_size(SOCK_DGRAM, SO_RCVBUF);
    return cached_size;
}

uint32_t default_tcp_sendbuf_size()
{
    static std::atomic<uint32_t> cached_size(0);

    if (cached_size != 0)
        return cached_size;

    cached_size = get_socket_buf_size(SOCK_STREAM, SO_SNDBUF);
    return cached_size;
}

uint32_t default_tcp_recvbuf_size()
{
    static std::atomic<uint32_t> cached_size(0);

    if (cached_size != 0)
        return cached_size;

    cached_size = get_socket_buf_size(SOCK_STREAM, SO_RCVBUF);
    return cached_size;
}

bool is_ip_address_useable(const IpAddress& ip, const std::vector<NetworkAdapter>& adapters)
{
    if (adapters.size() == 0)
        return false;

    if (!ip.IsValid())
        return false;

    if (ip.IsBroadcast() || ip.IsMulticast())
        return true;

    // IP ANY
    if (ip.HostOrderAddress() == 0)
        return true;

    for (auto& adapter : adapters)
    {
        // don't bother checking against adapters with null/invalid settings
        if (adapter.host_order_ip_addr == 0 || adapter.host_order_subnet_mask == 0)
            continue;

        if (are_ips_in_same_subnet(ip.HostOrderAddress(),
                adapter.host_order_ip_addr,
                adapter.host_order_subnet_mask))
        {
            return true;
        }
    }

    return false;
}

// this could probably use more thought...
bool ip_addresses_share_subnet(const IpAddress& ip1, const IpAddress& ip2, const IpAddress& subnet_mask)
{
    // subnet comparison doesn't apply to broadcast and multicast
    if (!ip1.IsValid() || ip1.IsBroadcast() || ip1.IsMulticast())
        return false;

    if (!ip2.IsValid() || ip2.IsBroadcast() || ip2.IsMulticast())
        return false;

    if (!subnet_mask.IsValid())
        return false;

    return are_ips_in_same_subnet(ip1.HostOrderAddress(),
        ip2.HostOrderAddress(),
        subnet_mask.HostOrderAddress());
}

IpAddress get_broadcast_addr(const IpAddress& ip, const std::vector<NetworkAdapter>& adapters)
{
    if (adapters.size() == 0)
        return IpAddress::IP_BROADCAST;

    if (!ip.IsValid())
        return IpAddress::IP_BROADCAST;

    if (ip.IsMulticast())
        return IpAddress(ip.HostOrderAddress());

    for (auto& adapter : adapters)
    {
        // don't bother checking against adapters with null/invalid settings
        if (adapter.host_order_ip_addr == 0 || adapter.host_order_subnet_mask == 0)
            continue;

        if (are_ips_in_same_subnet(ip.HostOrderAddress(),
                adapter.host_order_ip_addr,
                adapter.host_order_subnet_mask))
        {
            return IpAddress(adapter.host_order_broadcast_addr);
        }
    }

    return IpAddress::IP_BROADCAST;
}

std::string ip_to_string(uint32_t host_order_ip)
{
    auto network_order_address = htonl(host_order_ip);

    char str[INET_ADDRSTRLEN];
    auto result = inet_ntop(AF_INET, &network_order_address, str, INET_ADDRSTRLEN);
    if (result == NULL)
        return "";

    return std::string(str);
}

std::string ip_to_string(const IpAddress& ip)
{
    return ip_to_string(ip.HostOrderAddress());
}

SocketStatus socket_wait_for_send_ready(const NetSocket& socket, uint32_t timeout_milli)
{
    const SocketHandle socket_handle = socket.Handle();
    if (socket_handle == socket_impl::INVALID_SOCKET_HANDLE)
        return SocketStatus::Error;

    struct timeval timeout;
    timeout.tv_sec = timeout_milli / 1000;
    timeout.tv_usec = (timeout_milli % 1000) * 1000;

    fd_set writefds;
    FD_ZERO(&writefds);
    FD_SET(socket_handle, &writefds);

    int result = select((int)socket_handle + 1, NULL, &writefds, NULL, 
        (timeout_milli != SOCKET_INFINITE_TIMEOUT) ? &timeout : NULL);

    if (result > 0)
        return SocketStatus::Done;

    if (result == 0)
        return SocketStatus::NotReady; // timeout reached

    return socket_impl::error_code_as_status(socket_impl::last_system_error_code());
}

SocketStatus socket_wait_for_recv_ready(const NetSocket& socket, uint32_t timeout_milli)
{
    const SocketHandle socket_handle = socket.Handle();
    if (socket_handle == socket_impl::INVALID_SOCKET_HANDLE)
        return SocketStatus::Error;

    struct timeval timeout;
    timeout.tv_sec = timeout_milli / 1000;
    timeout.tv_usec = (timeout_milli % 1000) * 1000;

    fd_set readfds;
    FD_ZERO(&readfds);
    FD_SET(socket_handle, &readfds);

    int result = select((int)socket_handle + 1, &readfds, NULL, NULL, 
        (timeout_milli != SOCKET_INFINITE_TIMEOUT) ? &timeout : NULL);

    if (result > 0)
        return SocketStatus::Done;

    if (result == 0)
        return SocketStatus::NotReady; // timeout reached

    return socket_impl::error_code_as_status(socket_impl::last_system_error_code());
}

SocketStatus tcp_recv_with_timeout(TcpSocket& socket, uint8_t* data, std::size_t data_size,
    std::size_t& bytes_received, uint32_t timeout_milli)
{
    auto wait_status = socket_wait_for_recv_ready(socket, timeout_milli);
    if (wait_status == SocketStatus::Done)
        return socket.Receive(data, data_size, bytes_received);

    return wait_status;
}

SocketStatus udp_recv_with_timeout(UdpSocket& socket, uint8_t* data, std::size_t data_size,
    std::size_t& bytes_received, IpEndpoint& remote_endpoint, uint32_t timeout_milli)
{
    auto wait_status = socket_wait_for_recv_ready(socket, timeout_milli);
    if (wait_status == SocketStatus::Done)
        return socket.Receive(data, data_size, bytes_received, remote_endpoint);

    return wait_status;
}

std::pair<SocketStatus, uint32_t> try_send_all(TcpSocket& socket, uint8_t* data, size_t data_size,
    std::size_t& bytes_sent, uint32_t retry_count, uint32_t timeout_per_retry_milli)
{
    bytes_sent = 0;
    size_t bytes_sent_now = 0;
    SocketStatus send_status = socket.Send(data, data_size, bytes_sent_now);
    bytes_sent += bytes_sent_now;

    // if the first send returned Done or an Error, then skip retry code.
    if (retry_count == 0 ||
        (send_status != SocketStatus::NotReady && send_status != SocketStatus::Partial))
    {
        return std::make_pair(send_status, 0);
    }

    // First send failed, enter the retry logic.
    uint32_t actual_retry_count = 0;
    while (bytes_sent != data_size)
    {
        ++actual_retry_count;

        auto wait_status = socket_wait_for_send_ready(socket, timeout_per_retry_milli);

        if (wait_status == SocketStatus::Error)
            return std::make_pair(wait_status, actual_retry_count);

        // if socket is ready to write, try the send again
        if (wait_status == SocketStatus::Done)
        {
            bytes_sent_now = 0;
            send_status = socket.Send(data + bytes_sent, data_size - bytes_sent, bytes_sent_now);
            bytes_sent += bytes_sent_now;

            // if the send completed, stop
            if (send_status == SocketStatus::Done)
                break;

            // if there is an error, stop
            if (send_status == SocketStatus::Error ||
                send_status == SocketStatus::Disconnected)
            {
                break;
            }
        }

        // if the retry limit reached, stop
        if (actual_retry_count == retry_count)
        {
            send_status = (bytes_sent > 0) ? SocketStatus::Partial : SocketStatus::NotReady;
            break;
        }
    }

    return std::make_pair(send_status, actual_retry_count);
}

std::pair<SocketStatus, uint32_t> try_send_datagram(UdpSocket& socket, uint8_t* data, std::size_t data_size,
    const IpEndpoint& remote_endpoint, uint32_t retry_count, uint32_t timeout_per_retry_milli)
{
    SocketStatus send_status = (socket.IsConnected())
        ? socket.Send(data, data_size)
        : socket.Send(data, data_size, remote_endpoint);

    // if the first send returned Done or an Error, then skip retry code.
    if (retry_count == 0 || send_status != SocketStatus::NotReady)
        return std::make_pair(send_status, 0);

    // First send failed, enter the retry logic.
    uint32_t actual_retry_count = 0;
    while (send_status != SocketStatus::Done)
    {
        ++actual_retry_count;

        auto wait_status = socket_wait_for_send_ready(socket, timeout_per_retry_milli);

        if (wait_status == SocketStatus::Error)
            return std::make_pair(wait_status, actual_retry_count);

        // if socket is ready to write, try the send again
        if (wait_status == SocketStatus::Done)
        {
            send_status = (socket.IsConnected())
                ? socket.Send(data, data_size)
                : socket.Send(data, data_size, remote_endpoint);

            // if the send completed, stop
            if (send_status == SocketStatus::Done)
                break;

            // if there is an error, stop
            if (send_status == SocketStatus::Error)
                break;
        }

        // if the retry limit reached, stop
        if (actual_retry_count == retry_count)
            break;
    }

    return std::make_pair(send_status, actual_retry_count);
}

SocketStatus listen_and_connect(TcpListenSocket& listen_socket, IpEndpoint& listen_endpoint,
    TcpSocket& client_socket, uint32_t connect_timeout_milli)
{
    if (listen_socket.IsListening() && listen_endpoint != listen_socket.LocalEndpoint())
        listen_socket.Close();

    if (!listen_socket.IsListening())
    {
        auto listen_status = listen_socket.Listen(listen_endpoint);
        if (listen_status != SocketStatus::Done)
            return listen_status;
    }

    return listen_socket.Accept(client_socket, connect_timeout_milli);
}

SocketStatus listen_and_connect_single_client(TcpListenSocket& listen_socket, IpEndpoint& listen_endpoint,
    TcpSocket& client_socket, uint32_t connect_timeout_milli)
{
    auto accept_status = listen_and_connect(listen_socket, listen_endpoint,
        client_socket, connect_timeout_milli);

    // If connected, close the socket to reject any additional incoming connections.
    if (accept_status == SocketStatus::Done)
        listen_socket.Close();

    return accept_status;
}

} // namespace socket_utils
