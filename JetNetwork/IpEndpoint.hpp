#pragma once

#include <stdint.h>
#include <string>
#include "IpAddress.hpp"

class IpEndpoint
{
public:
private:
    IpAddress ip_addr;
    uint16_t port = AnyPort;

public:
    IpEndpoint() = default;
    IpEndpoint(const IpEndpoint&) = default;

    IpEndpoint(const IpAddress& ip_addr_in, uint16_t port_in)
        : ip_addr(ip_addr_in), port(port_in)
    {
    }

    IpEndpoint(const sockaddr_in& sockaddr_ep)
        : ip_addr(ntohl(sockaddr_ep.sin_addr.s_addr))
        , port(ntohs(sockaddr_ep.sin_port))
    {
    }

    inline const IpAddress& IpAddr() const { return ip_addr; }
    inline uint16_t Port() const { return port; }
    inline bool IsValid() const { return ip_addr.IsValid(); }

    std::string ToString() const
    {
        return (ip_addr.ToString().append(":")).append(std::to_string(port));
    }

    bool operator==(const IpEndpoint& other) const
    {
        return (ip_addr == other.ip_addr) && (port == other.port);
    }

    bool operator!=(const IpEndpoint& other) const
    {
        return !(*this == other);
    }
};
