#pragma once

#include <stdint.h>
#include <string>

class IpAddress
{
public:
    IpAddress() = default;
    IpAddress(const IpAddress&) = default;
    IpAddress(const std::string& address);
    IpAddress(uint32_t host_order_address);
    IpAddress(uint8_t a, uint8_t b, uint8_t c, uint8_t d);

    void SetIp(const std::string& address);
    void SetIp(uint32_t host_order_address);
    void SetIp(uint8_t a, uint8_t b, uint8_t c, uint8_t d);

    inline bool IsValid() const { return is_valid; }
    inline bool IsAny() const { return is_valid && ip_address == 0; }
    bool IsUnicast() const;
    bool IsBroadcast() const;
    bool IsMulticast() const;
    bool IsLoopback() const;

    inline uint32_t HostOrderAddress() const { return ip_address; }
    std::string ToString() const;

    bool operator==(const IpAddress& other) const { return (is_valid == other.is_valid) && (ip_address == other.ip_address); };
    bool operator!=(const IpAddress& other) const { return !(*this == other); };

private:
    bool is_valid = false;
    uint32_t ip_address = 0; // stored in host order

public:
    static const IpAddress IP_NONE;
    static const IpAddress IP_ANY;
    static const IpAddress IP_LOOPBACK;
    static const IpAddress IP_BROADCAST;
};
