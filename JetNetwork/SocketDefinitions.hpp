#pragma once
#include "PlatformImpl/PlatformDefines.h"
#include <stdint.h>
#include <string>

#ifdef WIN32
#include <basetsd.h>
typedef UINT_PTR SocketHandle;
// linux
#else
typedef int SocketHandle;
#endif

// The max packet size can be reduced from the max (65507) to fit different requirements.
// Ex. One could use the expected MTU to prevent fragmented packets from being sent.
//   IPv4 UDP: 1500 MTU - 20 IP hdr - 8 UDP hdr = 1472
constexpr size_t MAX_UDP_PACKET_SIZE = 65507;
constexpr uint32_t SOCKET_INFINITE_TIMEOUT = 0xFFFFFFFF;

enum class SocketStatus
{
    Done,         // The socket operation complete
    Partial,      // The socket didn't send/receive all the data requested
    NotReady,     // The socket is not ready to complete the operation
    Disconnected, // The socket has been disconnected
    Error         // An unexpected error happened
};

enum
{
    AnyPort = 0 // tell the system to pick any available port
};

struct NetworkAdapter
{
    std::string name;
    uint32_t host_order_ip_addr;
    uint32_t host_order_subnet_mask;
    uint32_t host_order_broadcast_addr;
};

// common ttl thresholds for mutlicast
enum class MutlicastTtlThreshold : uint8_t
{
    LocalHost = 0,       // Restricted to the same host. Won't be output by any interface.
    SameSubnet = 1,      // Restricted to the same subnet. Won't be forwarded by a router.
    SameSite = 31,       // Range(2 - 31) Restricted to the same site, organization or department.
    SameCountry = 63,    // Range(32 - 63) Restricted to the same region or country.
    SameContinent = 127, // Range(64 - 127) Restricted to the same continent.
    Unrestricted = 255,  // Range(128 - 255) Unrestricted in scope. Global.
};
