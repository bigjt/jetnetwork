#pragma once
#include "PlatformImpl/PlatformDefines.h"

#if defined(WIN32)
#include "PlatformImpl/Windows/WinSocketImpl.hpp"
#else
#include "PlatformImpl/Linux/LinuxSocketImpl.hpp"
#endif
