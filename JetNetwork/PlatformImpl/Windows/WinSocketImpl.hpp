#pragma once

#include "../../SocketDefinitions.hpp"
#include <winsock2.h>
#include <ws2tcpip.h>

#pragma comment(lib, "Ws2_32.lib") // link the winsock libary file

namespace socket_impl
{
constexpr SocketHandle INVALID_SOCKET_HANDLE = INVALID_SOCKET;
constexpr int TCP_SEND_RECV_FLAGS = 0;

void close_socket(SocketHandle sock);
void shutdown_socket(SocketHandle sock);
SocketStatus enable_blocking(SocketHandle sock, bool block);
SocketStatus error_code_as_status(int error_code);
int last_system_error_code();

// This function enables or disables a winsock only "feature" for UDP sockets that presents an
// ICMP "port unreachable" error when calling recv(). This happens whenever the socket sends
// a packet to a remote host that has the destination IP, but does not have the port open.
// Google this for more info: Windows UDP sockets, recvfrom() fails with error 10054
void enable_udp_icmp_recv_error(SocketHandle sock, bool enable);

} // namespace socket_impl
