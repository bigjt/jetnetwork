#include "WinSocketUtilsImpl.hpp"
#include "WinSocketImpl.hpp"
#include <iphlpapi.h>
#pragma comment(lib, "iphlpapi.lib")

namespace socket_utils
{
std::vector<NetworkAdapter> get_network_ipv4_adapters()
{
    DWORD size = 0;
    std::vector<NetworkAdapter> net_adapters;

    // capture size of the adapter data
    if (GetAdaptersInfo(nullptr, &size) != ERROR_BUFFER_OVERFLOW)
        return net_adapters;

    std::vector<uint8_t> adapter_buffer(size);
    if (GetAdaptersInfo((IP_ADAPTER_INFO*)&adapter_buffer[0], &size) != ERROR_SUCCESS)
        return net_adapters;

    // local/loopback address is not included in GetAdaptersInfo
    net_adapters.emplace_back(
        NetworkAdapter{
            "localhost",
            2130706433, // 127.0.0.1
            4278190080, // 255.0.0.0
            2147483647  // 127.255.255.255
        });

    IP_ADAPTER_INFO* adapter_info_list = (IP_ADAPTER_INFO*)&adapter_buffer[0];
    for (; adapter_info_list != nullptr; adapter_info_list = adapter_info_list->Next)
    {
        // only care about wired and wireless eth adapters
        if (adapter_info_list->Type != MIB_IF_TYPE_ETHERNET &&
            adapter_info_list->Type != IF_TYPE_IEEE80211)
        {
            continue;
        }

        // each adapter may have multiple IPs
        IP_ADDR_STRING* ip_address_list = &adapter_info_list->IpAddressList;
        for (; ip_address_list != nullptr; ip_address_list = ip_address_list->Next)
        {
            u_long ip = 0;
            u_long mask = 0;
            u_long broadcastip = 0xFFFFFFFF;

            inet_pton(AF_INET, ip_address_list->IpAddress.String, &ip);
            inet_pton(AF_INET, ip_address_list->IpMask.String, &mask);
            broadcastip = ip | ~mask;

            char broadcast_str[INET_ADDRSTRLEN];
            inet_ntop(AF_INET, &broadcastip, broadcast_str, INET_ADDRSTRLEN);

            net_adapters.emplace_back(
                NetworkAdapter{
                    std::string(adapter_info_list->Description),
                    ntohl(ip),
                    ntohl(mask),
                    ntohl(broadcastip) });
        }
    }

    return net_adapters;
}

std::string error_code_to_string(int socket_error_code)
{
#define CASE_CODE_TO_STRING(code) \
    case code: return #code;
    switch (socket_error_code)
    {
        CASE_CODE_TO_STRING(WSA_INVALID_HANDLE)
        CASE_CODE_TO_STRING(WSA_NOT_ENOUGH_MEMORY)
        CASE_CODE_TO_STRING(WSA_INVALID_PARAMETER)
        CASE_CODE_TO_STRING(WSA_OPERATION_ABORTED)
        CASE_CODE_TO_STRING(WSA_IO_INCOMPLETE)
        CASE_CODE_TO_STRING(WSA_IO_PENDING)
        CASE_CODE_TO_STRING(WSAEINTR)
        CASE_CODE_TO_STRING(WSAEBADF)
        CASE_CODE_TO_STRING(WSAEACCES)
        CASE_CODE_TO_STRING(WSAEFAULT)
        CASE_CODE_TO_STRING(WSAEINVAL)
        CASE_CODE_TO_STRING(WSAEMFILE)
        CASE_CODE_TO_STRING(WSAEWOULDBLOCK)
        CASE_CODE_TO_STRING(WSAEINPROGRESS) 
        CASE_CODE_TO_STRING(WSAEALREADY)
        CASE_CODE_TO_STRING(WSAENOTSOCK)
        CASE_CODE_TO_STRING(WSAEDESTADDRREQ)
        CASE_CODE_TO_STRING(WSAEMSGSIZE)
        CASE_CODE_TO_STRING(WSAEPROTOTYPE)
        CASE_CODE_TO_STRING(WSAENOPROTOOPT)
        CASE_CODE_TO_STRING(WSAEPROTONOSUPPORT)
        CASE_CODE_TO_STRING(WSAESOCKTNOSUPPORT)
        CASE_CODE_TO_STRING(WSAEOPNOTSUPP)
        CASE_CODE_TO_STRING(WSAEPFNOSUPPORT)
        CASE_CODE_TO_STRING(WSAEAFNOSUPPORT)
        CASE_CODE_TO_STRING(WSAEADDRINUSE)
        CASE_CODE_TO_STRING(WSAEADDRNOTAVAIL)
        CASE_CODE_TO_STRING(WSAENETDOWN)
        CASE_CODE_TO_STRING(WSAENETUNREACH)
        CASE_CODE_TO_STRING(WSAENETRESET)
        CASE_CODE_TO_STRING(WSAECONNABORTED)
        CASE_CODE_TO_STRING(WSAECONNRESET)
        CASE_CODE_TO_STRING(WSAENOBUFS)
        CASE_CODE_TO_STRING(WSAEISCONN)
        CASE_CODE_TO_STRING(WSAENOTCONN)
        CASE_CODE_TO_STRING(WSAESHUTDOWN)
        CASE_CODE_TO_STRING(WSAETOOMANYREFS)
        CASE_CODE_TO_STRING(WSAETIMEDOUT)
        CASE_CODE_TO_STRING(WSAECONNREFUSED)
        CASE_CODE_TO_STRING(WSAELOOP)
        CASE_CODE_TO_STRING(WSAENAMETOOLONG)
        CASE_CODE_TO_STRING(WSAEHOSTDOWN)
        CASE_CODE_TO_STRING(WSAEHOSTUNREACH)
        CASE_CODE_TO_STRING(WSAENOTEMPTY)
        CASE_CODE_TO_STRING(WSAEPROCLIM)
        CASE_CODE_TO_STRING(WSAEUSERS)
        CASE_CODE_TO_STRING(WSAEDQUOT)
        CASE_CODE_TO_STRING(WSAESTALE)
        CASE_CODE_TO_STRING(WSAEREMOTE)
        CASE_CODE_TO_STRING(WSASYSNOTREADY)
        CASE_CODE_TO_STRING(WSAVERNOTSUPPORTED)
        CASE_CODE_TO_STRING(WSANOTINITIALISED)
        CASE_CODE_TO_STRING(WSAEDISCON)
        CASE_CODE_TO_STRING(WSAENOMORE)
        CASE_CODE_TO_STRING(WSAECANCELLED)
        CASE_CODE_TO_STRING(WSAEINVALIDPROCTABLE)
        CASE_CODE_TO_STRING(WSAEINVALIDPROVIDER)
        CASE_CODE_TO_STRING(WSAEPROVIDERFAILEDINIT)
        CASE_CODE_TO_STRING(WSASYSCALLFAILURE)
        CASE_CODE_TO_STRING(WSASERVICE_NOT_FOUND)
        CASE_CODE_TO_STRING(WSATYPE_NOT_FOUND)
        CASE_CODE_TO_STRING(WSA_E_NO_MORE)
        CASE_CODE_TO_STRING(WSA_E_CANCELLED)
        CASE_CODE_TO_STRING(WSAEREFUSED)
        CASE_CODE_TO_STRING(WSAHOST_NOT_FOUND)
        CASE_CODE_TO_STRING(WSATRY_AGAIN)
        CASE_CODE_TO_STRING(WSANO_RECOVERY)
        CASE_CODE_TO_STRING(WSANO_DATA)
        CASE_CODE_TO_STRING(NO_ERROR)
    }

    // default
    return std::to_string(socket_error_code);
}

std::string error_code_to_descriptive_string(int socket_error_code)
{
    char msg_buf[256];
    std::string out_string(error_code_to_string(socket_error_code));
    out_string += " (";
    out_string += std::to_string(socket_error_code);
    out_string += ") ";

    // https://docs.microsoft.com/en-us/windows/desktop/api/winbase/nf-winbase-formatmessage
    auto num_chars_copied = FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL, socket_error_code, NULL, msg_buf, sizeof(msg_buf), NULL);

    if (num_chars_copied > 0)
        out_string += msg_buf;

    return out_string;
}

bool send_arp_request(const IpAddress& remote_ip, const IpAddress& local_ip)
{
    if (!remote_ip.IsValid() || !local_ip.IsValid())
        return false;

    ULONG mac_addr[2] = { 0 };
    ULONG mac_addr_len = 6; // default length of six bytes (msdn code)
    ULONG dst_ip = htonl(remote_ip.HostOrderAddress());
    ULONG src_ip = htonl(local_ip.HostOrderAddress());

    auto retval = SendARP(dst_ip, src_ip, &mac_addr, &mac_addr_len);
    return retval == NO_ERROR;
}

} // namespace socket_utils
