#pragma once
#include "../../SocketDefinitions.hpp"
#include <vector>
#include <string>
#include "../../IpAddress.hpp"

namespace socket_utils
{
std::vector<NetworkAdapter> get_network_ipv4_adapters();
std::string error_code_to_string(int socket_error_code);
std::string error_code_to_descriptive_string(int socket_error_code);

// send an arp request to the remote_ip.
//   returns true if the remote address is available in the ARP table.
// this function blocks up to 3 - 5 seconds waiting for a ARP reply.
bool send_arp_request(const IpAddress& remote_ip, const IpAddress& local_ip = IpAddress::IP_ANY);

} // namespace socket_utils
