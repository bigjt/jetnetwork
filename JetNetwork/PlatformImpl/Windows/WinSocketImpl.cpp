#include "WinSocketImpl.hpp"
#include <thread>
#include <atomic>
#include <MSWSock.h>

namespace
{
struct WinsockResourceAllocator
{
    std::atomic<bool> retry_thread_running = false;
    std::thread retry_wsa_startup_thread;

    WinsockResourceAllocator()
    {
        WORD wVersionRequested = MAKEWORD(2, 2);
        WSADATA wsaData;
        if (WSAStartup(wVersionRequested, &wsaData) != 0)
        {
            // Generally this shouldn't be needed.
            // BUT, when apps are loaded during windows startup, the network may not be ready right away.
            retry_thread_running = true;
            retry_wsa_startup_thread = std::thread(&WinsockResourceAllocator::RetryWSAStartup, this, wVersionRequested, wsaData);
        }
    }

    void RetryWSAStartup(WORD versionRequested, WSADATA wsaData)
    {
        // Considered adding a retry limit to run for X seconds before throwing an exception with error_code_to_descriptive_string().
        // BUT, I want the give the calling app more control of handling an uninitialized network.

        while (retry_thread_running)
        {
            if (WSAStartup(versionRequested, &wsaData) == 0)
            {
                retry_thread_running = false;
                return;
            }

            Sleep(100);
        }
    }

    ~WinsockResourceAllocator()
    {
        retry_thread_running = false;
        if (retry_wsa_startup_thread.joinable())
            retry_wsa_startup_thread.join();

        WSACleanup();
    }
};

// statically setup the networking code and clean up when the program terminates.
WinsockResourceAllocator network_resource_allocator;

} // namespace

namespace socket_impl
{
void close_socket(SocketHandle sock)
{
    closesocket(sock);
}

void shutdown_socket(SocketHandle sock)
{
    shutdown(sock, SD_BOTH);
}

void enable_udp_icmp_recv_error(SocketHandle sock, bool enable)
{
    BOOL enable_option = enable;
    DWORD bytes_returned = 0;
    WSAIoctl(sock, SIO_UDP_CONNRESET, &enable_option, sizeof(enable_option), NULL, 0, &bytes_returned, NULL, NULL);
}

SocketStatus enable_blocking(SocketHandle sock, bool block)
{
    u_long blocking = block ? 0 : 1;
    if (ioctlsocket(sock, FIONBIO, &blocking) != 0)
        return SocketStatus::Error;

    return SocketStatus::Done;
}

SocketStatus error_code_as_status(int error_code)
{
    switch (error_code)
    {
    case WSAEWOULDBLOCK:  return SocketStatus::NotReady;
    case WSAEALREADY:     return SocketStatus::NotReady;
    case WSAECONNABORTED: return SocketStatus::Disconnected;
    case WSAECONNRESET:   return SocketStatus::Disconnected;
    case WSAETIMEDOUT:    return SocketStatus::Disconnected;
    case WSAENETRESET:    return SocketStatus::Disconnected;
    case WSAENOTCONN:     return SocketStatus::Disconnected;
    // after calling connect, calling connect again will return WSAEISCONN (connect may still be in progress for non-blocking sockets)
    // just return not ready so the Socket code can verify the connection is good.
    case WSAEISCONN:      return SocketStatus::NotReady;
    default:              return SocketStatus::Error;
    }
}

int last_system_error_code()
{
    return WSAGetLastError();
}

} // namespace socket_impl
