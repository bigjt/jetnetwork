#pragma once
#include "../../SocketDefinitions.hpp"
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

namespace socket_impl
{
constexpr SocketHandle INVALID_SOCKET_HANDLE = -1;
constexpr int TCP_SEND_RECV_FLAGS = MSG_NOSIGNAL; // MSG_NOSIGNAL prevents SIGPIPE from being thrown on disconnect.

void close_socket(SocketHandle sock);
void shutdown_socket(SocketHandle sock);
SocketStatus enable_blocking(SocketHandle sock, bool block);
SocketStatus error_code_as_status(int error_code);
int last_system_error_code();
} // namespace socket_impl
