#pragma once
#include "../../SocketDefinitions.hpp"
#include <vector>
#include <string>

namespace socket_utils
{
std::vector<NetworkAdapter> get_network_ipv4_adapters();
std::string error_code_to_string(int socket_error_code);
std::string error_code_to_descriptive_string(int socket_error_code);
} // namespace socket_utils
