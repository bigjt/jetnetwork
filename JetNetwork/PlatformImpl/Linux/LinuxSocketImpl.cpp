#include "LinuxSocketImpl.hpp"

#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>

namespace socket_impl
{
void close_socket(SocketHandle socketHandle)
{
    close(socketHandle);
}

void shutdown_socket(SocketHandle sock)
{
    shutdown(sock, SHUT_RDWR);
}

SocketStatus enable_blocking(SocketHandle sock, bool block)
{
    int status = fcntl(sock, F_GETFL);
    if (block)
    {
        if (fcntl(sock, F_SETFL, status & ~O_NONBLOCK) == -1)
            return SocketStatus::Error;
    }
    else
    {
        if (fcntl(sock, F_SETFL, status | O_NONBLOCK) == -1)
            return SocketStatus::Error;
    }

    return SocketStatus::Done;
}

SocketStatus error_code_as_status(int error_code)
{
    if ((error_code == EAGAIN) || (error_code == EINPROGRESS))
        return SocketStatus::NotReady;

    switch (error_code)
    {
    case EWOULDBLOCK:  return SocketStatus::NotReady;
    case ECONNABORTED: return SocketStatus::Disconnected;
    case ECONNRESET:   return SocketStatus::Disconnected;
    case ETIMEDOUT:    return SocketStatus::Disconnected;
    case ENETRESET:    return SocketStatus::Disconnected;
    case ENOTCONN:     return SocketStatus::Disconnected;
    case EPIPE:        return SocketStatus::Disconnected;
    // Treating EALREADY like a error speeds up the connection process a lot. 
        // Not sure if it is correct to request a close socket just to reopen to try again.
    //case EALREADY:     return SocketStatus::NotReady;
    default:           return SocketStatus::Error;
    }
}
int last_system_error_code()
{
    return errno;
}
} // namespace socket_impl
