#include "LinuxSocketUtilsImpl.hpp"
#include "LinuxSocketImpl.hpp"
#include <fcntl.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <string.h>

namespace socket_utils
{
std::vector<NetworkAdapter> get_network_ipv4_adapters()
{
    struct ifaddrs *ifaddr, *ifa;
    std::vector<NetworkAdapter> net_adapters;

    if (getifaddrs(&ifaddr) == -1)
        return net_adapters;

    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
    {
        if (ifa->ifa_addr == NULL)
            continue;

        // ipv4 only
        if (ifa->ifa_addr->sa_family == AF_INET)
        {
            auto network_order_ip = ((struct sockaddr_in*)ifa->ifa_addr)->sin_addr.s_addr;
            auto network_order_mask = ((struct sockaddr_in*)ifa->ifa_netmask)->sin_addr.s_addr;
            auto network_order_bcast = network_order_ip | ~network_order_mask;

            if (network_order_bcast == 0)
                network_order_bcast = 0xFFFFFFFF;

            NetworkAdapter adapter{
                .name = ifa->ifa_name,
                .host_order_ip_addr = ntohl(network_order_ip),
                .host_order_subnet_mask = ntohl(network_order_mask),
                .host_order_broadcast_addr = ntohl(network_order_bcast),
                // linux adapters have this extra ifu_broadaddr address field.
                //  calculating using the ip and netmask SHOULD work fine on most sytems.
                //.host_order_broadcast_addr = ntohl((ifa->ifa_flags & IFF_BROADCAST)
                //    ? ((struct sockaddr_in *)ifa->ifa_ifu.ifu_broadaddr)->sin_addr.s_addr
                //    : 0xFFFFFFFF),
            };

            net_adapters.push_back(adapter);
        }
    }

    freeifaddrs(ifaddr);
    return net_adapters;
}

// TODO: A descriptive string would be nice, but it looks like the errors mean different things depending on socket call used.
//       The caller of the NetSocket classes SHOULD log the function that threw the error code. (Connect, Send, Recv, etc).
//       to make troubleshooting easier.
//       strerror_r kinda sucks, the descriptive strings may need to be done by hand.
std::string error_code_to_string(int socket_error_code)
{
#define NO_ERROR 0
#define CASE_CODE_TO_STRING(code) \
    case code: return #code;
    switch (socket_error_code)
    {
        CASE_CODE_TO_STRING(EALREADY)
        CASE_CODE_TO_STRING(EINPROGRESS)
        CASE_CODE_TO_STRING(EWOULDBLOCK)
        CASE_CODE_TO_STRING(ETIMEDOUT)
        CASE_CODE_TO_STRING(EPIPE)
        CASE_CODE_TO_STRING(ENOTSOCK)
        CASE_CODE_TO_STRING(EDESTADDRREQ)
        CASE_CODE_TO_STRING(EMSGSIZE)
        CASE_CODE_TO_STRING(EPROTOTYPE)
        CASE_CODE_TO_STRING(ENOPROTOOPT)
        CASE_CODE_TO_STRING(EPROTONOSUPPORT)
        CASE_CODE_TO_STRING(ESOCKTNOSUPPORT)
        CASE_CODE_TO_STRING(EOPNOTSUPP)
        CASE_CODE_TO_STRING(EPFNOSUPPORT)
        CASE_CODE_TO_STRING(EAFNOSUPPORT)
        CASE_CODE_TO_STRING(EADDRINUSE)
        CASE_CODE_TO_STRING(EADDRNOTAVAIL)
        CASE_CODE_TO_STRING(ENETDOWN)
        CASE_CODE_TO_STRING(ENETUNREACH)
        CASE_CODE_TO_STRING(ENETRESET)
        CASE_CODE_TO_STRING(ECONNABORTED)
        CASE_CODE_TO_STRING(ECONNRESET)
        CASE_CODE_TO_STRING(ENOBUFS)
        CASE_CODE_TO_STRING(EISCONN)
        CASE_CODE_TO_STRING(ENOTCONN)
        CASE_CODE_TO_STRING(ESHUTDOWN)
        CASE_CODE_TO_STRING(ETOOMANYREFS)
        CASE_CODE_TO_STRING(ECONNREFUSED)
        CASE_CODE_TO_STRING(EHOSTDOWN)
        CASE_CODE_TO_STRING(EHOSTUNREACH)
        CASE_CODE_TO_STRING(EBADFD)
        CASE_CODE_TO_STRING(EINVAL)
        CASE_CODE_TO_STRING(ENOMEM)
        CASE_CODE_TO_STRING(ENAMETOOLONG)
        CASE_CODE_TO_STRING(ELOOP)
        CASE_CODE_TO_STRING(EFAULT)
        CASE_CODE_TO_STRING(EROFS)
        CASE_CODE_TO_STRING(NO_ERROR)
    }

    return std::to_string(socket_error_code);
}

std::string error_code_to_descriptive_string(int socket_error_code)
{
    // strerror_r is thread safe, but requires some extra work to make it crossplatform compliant.
    // this function isn't really THAT descriptive.
    return error_code_to_string(socket_error_code)
        .append(" (")
        .append(std::to_string(socket_error_code))
        .append(")");
}
} // namespace socket_utils
