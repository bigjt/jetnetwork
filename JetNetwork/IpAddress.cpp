#include "IpAddress.hpp"
#include "SocketImpl.hpp"
#include "SocketUtilsImpl.hpp"

#ifdef WIN32
#define IPADDR_HOST_IS_LITTLE_ENDIAN true
#else
#include <endian.h>
#define IPADDR_HOST_IS_LITTLE_ENDIAN __BYTE_ORDER == __LITTLE_ENDIAN 
#endif

const IpAddress IpAddress::IP_NONE;
const IpAddress IpAddress::IP_ANY(0, 0, 0, 0);
const IpAddress IpAddress::IP_LOOPBACK(127, 0, 0, 1);
const IpAddress IpAddress::IP_BROADCAST(255, 255, 255, 255);

namespace
{
bool validate_ip(const std::string& address, uint32_t& out_address)
{
    if (address.empty())
        return false;

    auto valid_ip = inet_pton(AF_INET, address.c_str(), &out_address) > 0;

    out_address = ntohl(out_address);
    return valid_ip;
}
} // namespace

IpAddress::IpAddress(const std::string& address)
{
    SetIp(address);
}

IpAddress::IpAddress(uint32_t host_order_address)
    : is_valid(true), ip_address(host_order_address)
{
}

IpAddress::IpAddress(uint8_t a, uint8_t b, uint8_t c, uint8_t d)
{
    SetIp(a, b, c, d);
}

void IpAddress::SetIp(const std::string& address)
{
    uint32_t ip = 0;
    is_valid = validate_ip(address, ip);
    ip_address = (is_valid) ? ip : 0; 
}

// uint32_t covers all valid values for a ipv4 address
// so we can assume the address can be resolved into a valid ip.
void IpAddress::SetIp(uint32_t host_order_address)
{
    is_valid = true;
    ip_address = host_order_address;
}

void IpAddress::SetIp(uint8_t a, uint8_t b, uint8_t c, uint8_t d)
{
#if IPADDR_HOST_IS_LITTLE_ENDIAN
    auto ip = ((uint32_t)(a) << 24) | ((uint32_t)(b) << 16) | ((uint32_t)(c) << 8) | ((uint32_t)(d));
#else
    auto ip = ((uint32_t)(d) << 24) | ((uint32_t)(c) << 16) | ((uint32_t)(b) << 8) | ((uint32_t)(a));
#endif
    SetIp(ip);
}

bool IpAddress::IsUnicast() const
{
    return is_valid && ip_address != INADDR_ANY && !IsBroadcast() && !IsMulticast();
}

bool IpAddress::IsBroadcast() const
{
    // Lazy broadcast address detection. Need to capture the subnetmask on the adapter to know for sure.
    // Check if last octet is 255. 
#if IPADDR_HOST_IS_LITTLE_ENDIAN
    return !IsMulticast() && ((ip_address & 0x000000FF) == 0x000000FF);
#else
    return !IsMulticast() && ((ip_address & 0xFF000000) == 0xFF000000);
#endif
}

bool IpAddress::IsMulticast() const
{
    // check first octet for value between 224 and 239.
#if IPADDR_HOST_IS_LITTLE_ENDIAN
    return ((ip_address & 0xF0000000) == 0xE0000000);
#else
    return ((ip_address & 0x000000F0) == 0x000000E0);
#endif
}

bool IpAddress::IsLoopback() const
{
    return ip_address == INADDR_LOOPBACK;
}

std::string IpAddress::ToString() const
{
    return socket_utils::ip_to_string(ip_address);
}
