#pragma once
#include "PlatformImpl/PlatformDefines.h"

#if defined(WIN32)
#include "PlatformImpl/Windows/WinSocketUtilsImpl.hpp"
#else
#include "PlatformImpl/Linux/LinuxSocketUtilsImpl.hpp"
#endif

#include <vector>
#include <string>
#include <utility>

class IpAddress;
class IpEndpoint;
class NetSocket;
class TcpSocket;
class UdpSocket;
class TcpListenSocket;
namespace socket_utils
{
// Having these util functions accept a std::vector<NetworkAdapter> as an argument is to give the caller full control
// of the caching/storage of the adapter info (created from socket_utils::get_network_ipv4_adapters()).

// returns true if the IP is useable from any of the provided NetworkAdapters
bool is_ip_address_useable(const IpAddress& ip, const std::vector<NetworkAdapter>& adapters);

// returns true if ip1 and ip2 are in the same subnet
bool ip_addresses_share_subnet(const IpAddress& ip1, const IpAddress& ip2, const IpAddress& subnet_mask);
bool ip_addresses_share_subnet(const IpAddress& ip1, const IpAddress& ip2, const std::vector<NetworkAdapter>& adapters);

// return the broadcast address based on the given ip and provided NetworkAdapters
IpAddress get_broadcast_addr(const IpAddress& ip, const std::vector<NetworkAdapter>& adapters);

std::string ip_to_string(uint32_t host_order_ip);
std::string ip_to_string(const IpAddress& ip);

// get the system default UDP socket send buffer size.
//   returns 0 if the network code isn't initalized yet.
uint32_t default_udp_sendbuf_size();

// get the system default UDP socket recv buffer size.
//   returns 0 if the network code isn't initalized yet.
uint32_t default_udp_recvbuf_size();

// get the system default TCP socket send buffer size.
//   returns 0 if the network code isn't initalized yet.
uint32_t default_tcp_sendbuf_size();

// get the system default TCP socket recv buffer size.
//   returns 0 if the network code isn't initalized yet.
uint32_t default_tcp_recvbuf_size();

// Waits until the socket is writable or until the timeout interval is reached.
// Use SOCKET_INFINITE_TIMEOUT for an infinite timeout.
//   returns Done when socket is ready.
//   returns NotReady when timeout is reached.
//   returns Error when socket has an error.
SocketStatus socket_wait_for_send_ready(const NetSocket& socket, uint32_t timeout_milli = SOCKET_INFINITE_TIMEOUT);

// Waits until the socket is readable or until the timeout interval is reached.
// Use SOCKET_INFINITE_TIMEOUT for an infinite timeout.
//   returns Done when socket is ready.
//   returns NotReady when timeout is reached.
//   returns Error when socket has an error.
SocketStatus socket_wait_for_recv_ready(const NetSocket& socket, uint32_t timeout_milli = SOCKET_INFINITE_TIMEOUT);

// Use SOCKET_INFINITE_TIMEOUT for an infinite timeout.
// Be mindful of using this with a blocking socket, the recv call may extend past the timeout duration.
//   returns Done or Partial when socket is reads data from socket.
//   returns NotReady when timeout is reached.
//   returns Error when socket has an error.
SocketStatus tcp_recv_with_timeout(TcpSocket& socket, uint8_t* data, std::size_t data_size,
    std::size_t& bytes_received, uint32_t timeout_milli);

// Use SOCKET_INFINITE_TIMEOUT for an infinite timeout
//   returns Done or Partial when socket is reads data from socket.
//   returns NotReady when timeout is reached.
//   returns Error when socket has an error.
SocketStatus udp_recv_with_timeout(UdpSocket& socket, uint8_t* data, std::size_t data_size,
    std::size_t& bytes_received, IpEndpoint& remote_endpoint, uint32_t timeout_milli);

// Use SOCKET_INFINITE_TIMEOUT for an infinite timeout.
// attempts to send all the data to the connected tcp socket.
//  returns SocketStatus and actual retry count so the caller can track potential send buffer full events.
std::pair<SocketStatus, uint32_t> try_send_all(TcpSocket& socket, uint8_t* data, size_t data_size,
    std::size_t& bytes_sent, uint32_t retry_count = 3, uint32_t timeout_per_retry_milli = 10);

std::pair<SocketStatus, uint32_t> try_send_datagram(UdpSocket& socket, uint8_t* data, std::size_t data_size,
    const IpEndpoint& remote_endpoint, uint32_t retry_count = 3, uint32_t timeout_per_retry_milli = 10);

// Listen, accept, and connect.
// listen_socket will listen on the provided listen_endpoint, then accept and establish the connection through the client_socket.
// SocketStatus returned is the status of the listen_socket, so use LastSocketErrorCode() on the listen socket if return != Done.
SocketStatus listen_and_connect(TcpListenSocket& listen_socket, IpEndpoint& listen_endpoint,
    TcpSocket& client_socket, uint32_t connect_timeout_milli = 0);

// Listen, accept, and connect.
// listen_socket will listen on the provided listen_endpoint, then accept and establish the connection through the client_socket.
// SocketStatus returned is the status of the listen_socket, so use LastSocketErrorCode() on the listen socket if return != Done.
// Once a single client has connected, the listen_socket will automatically be closed.
SocketStatus listen_and_connect_single_client(TcpListenSocket& listen_socket, IpEndpoint& listen_endpoint,
    TcpSocket& client_socket, uint32_t connect_timeout_milli = 0);

} // namespace socket_utils
